Package: ca-certificates
Version: 20210119
Architecture: all
Maintainer: Julien Cristau <jcristau@debian.org>
Installed-Size: 382
Depends: openssl (>= 1.1.1), debconf (>= 0.5) | debconf-2.0
Enhances: openssl
Breaks: ca-certificates-java (<< 20121112+nmu1)
Multi-Arch: foreign
Priority: optional
Section: misc
Filename: pool/main/c/ca-certificates/ca-certificates_20210119_all.deb
Size: 158288
SHA256: b2d488ad4d8d8adb3ba319fc9cb2cf9909fc42cb82ad239a26c570a2e749c389
SHA1: ef0ce467f4982ebbd31b6e0e4a87d71b049b218a
MD5sum: d6e49d96653ee62bb40fab646be12e03
Description: Common CA certificates
 Contains the certificate authorities shipped with Mozilla's browser to allow
 SSL-based applications to check for the authenticity of SSL connections.
 .
 Please note that Debian can neither confirm nor deny whether the
 certificate authorities whose certificates are included in this package
 have in any way been audited for trustworthiness or RFC 3647 compliance.
 Full responsibility to assess them belongs to the local system
 administrator.

Package: desktop-base
Version: 1:11.0.4~peppermint7
Architecture: all
Maintainer: Debian Desktop Team <debian-desktop@lists.debian.org>
Installed-Size: 17393
Depends: librsvg2-common, fonts-quicksand
Recommends: plymouth-label
Suggests: gnome | kde-standard | xfce4 | wmaker
Homepage: https://www.debian.org/devel/debian-desktop/
Priority: optional
Section: x11
Filename: pool/main/d/desktop-base/desktop-base_11.0.4~peppermint7_all.deb
Size: 4833828
SHA256: ea0e98f7146b5be77bb1e1ece3ad852664e8cf931a707ecda924fb6887658f4e
SHA1: ddeb3cec0ca6ff1e3c6642de3538f55062cd3bfd
MD5sum: 7eb8410bf7773b7600644900facf26d3
Description: common files for the Debian Desktop
 This package contains various miscellaneous files which are used by
 Debian Desktop installations.  Currently, it provides some
 Debian-related artwork and themes, .desktop files containing links to
 Debian related material (suitable for placement on a user's desktop),
 and other common files between the available desktop environments
 such as GNOME and KDE.

Package: devuan-keyring
Version: 2022.09.04
Architecture: all
Bugs: mailto:bugs@devuan.org
Maintainer: Veteran UNIX Admins <vua@devuan.org>
Installed-Size: 67
Depends: gpgv
Recommends: gnupg | gnupg1
Breaks: apt (<< 0.7.25.1)
Multi-Arch: foreign
Homepage: http://www.devuan.org
Priority: important
Section: misc
Filename: pool/main/d/devuan-keyring/devuan-keyring_2022.09.04_all.deb
Size: 36068
SHA256: 96c4a206e8dfdc21138ec619687ef9acf36e1524dd39190c040164f37cc3468d
SHA1: 79998bdf3f808cf6a0d328eb545270f53b3e8463
MD5sum: 6209781a66b39c95c012765bc7ca2297
Description: GnuPG archive key of the devuan repository
 The devuan repository digitally signs its Release files.
 This package contains the repository keys from devuan
 developers used for that.

Package: gnome-software-common
Source: gnome-software
Version: 40.4-0peppermint
Architecture: all
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Installed-Size: 4677
Multi-Arch: foreign
Homepage: https://wiki.gnome.org/Apps/Software
Priority: optional
Section: gnome
Filename: pool/main/g/gnome-software/gnome-software-common_40.4-0peppermint_all.deb
Size: 932548
SHA256: aa6fe07d71ca5bcaf12202e5135c420890a25d0f1ac17ce7c50a65b8befd7469
SHA1: 822162c8e84dc99240622fdf4c7d105166e64a3e
MD5sum: b4ed0da1252831540cd958fbffa917be
Description: Software Center for GNOME (common files)
 Software lets you install and update applications and system extensions.
 .
 Software uses a plugin architecture to separate the frontend from the
 technologies that are used underneath. Currently, a PackageKit plugin provides
 data from a number of traditional packaging systems, such as rpm or apt. An
 appdata plugin provides additional metadata from locally installed data in the
 appdata format.
 .
 This package contains the architecture-independent files.

Package: gnome-software-doc
Source: gnome-software
Version: 40.4-0peppermint
Architecture: all
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Installed-Size: 1100
Multi-Arch: foreign
Homepage: https://wiki.gnome.org/Apps/Software
Priority: optional
Section: doc
Filename: pool/main/g/gnome-software/gnome-software-doc_40.4-0peppermint_all.deb
Size: 242780
SHA256: da88234d15a0655c0ae45a5951b2c68f2a705fb8a3cca5ef7732e175045292f0
SHA1: 60e7c7e8dc292737949df60086185ef72fbc50a3
MD5sum: a0516c036d7982dff3be83c2a1aaeec4
Description: Software Center for GNOME - documentation
 Software lets you install and update applications and system extensions.
 .
 This package contains documentation for use when developing plugins for
 Software.

Package: hpijs-ppds
Source: hplip
Version: 3.21.4+dfsg0-1~peppermint1
Architecture: all
Maintainer: Steven Pusser <stevep@mxlinux.org>
Installed-Size: 112
Depends: cups-filters | foomatic-filters, printer-driver-hpijs (>= 3.21.4+dfsg0-1~peppermint1), python3:any, xz-utils
Breaks: foomatic-filters-ppds (>> 20000101), hplip-ppds
Replaces: hplip-ppds
Provides: hplip-ppds
Homepage: https://developers.hp.com/hp-linux-imaging-and-printing
Priority: optional
Section: utils
Filename: pool/main/h/hplip/hpijs-ppds_3.21.4+dfsg0-1~peppermint1_all.deb
Size: 94448
SHA256: c6c1413009ec066943cc35256148799acd37e4a3603b9950ea6fd087ea5dcdcd
SHA1: 737e5f50a565d7f5f841ecb08af732ccda9b37b5
MD5sum: 6f7953842fcb82e9243deed3b85dd2ef
Description: HP Linux Printing and Imaging - HPIJS PPD files
 This package contains PPD (printer definition) files for the
 printers supported through the HP Linux Printing and Imaging
 System HPIJS driver.
 .
 These PPDs should work well with the matching versions of HPLIP
 and HPIJS, but may not be the most up-to-date PPDs available for
 a given printer.  See http://www.openprinting.org/ for the latest
 version of the PPDs (which are not guaranteed to work well).
Original-Maintainer: Debian Printing Team <debian-printing@lists.debian.org>

Package: hplip-data
Source: hplip
Version: 3.21.4+dfsg0-1~peppermint1
Architecture: all
Maintainer: Steven Pusser <stevep@mxlinux.org>
Installed-Size: 10292
Depends: python3-distro, xz-utils, python3:any
Suggests: hplip
Homepage: https://developers.hp.com/hp-linux-imaging-and-printing
Priority: optional
Section: utils
Filename: pool/main/h/hplip/hplip-data_3.21.4+dfsg0-1~peppermint1_all.deb
Size: 6620376
SHA256: a7066f23d69007a93e4811e665e22b5bad16454b01023ca1411d79a95ebb2e82
SHA1: 66d976adaeee535b35b13ccf42634f6d956b5f0e
MD5sum: 97ce0a46fbaa8b30d6e4cb5beaca0a9f
Description: HP Linux Printing and Imaging - data files
 The HP Linux Printing and Imaging System provides full support for
 printing on most HP SFP (single function peripheral) inkjets and many
 LaserJets, and for scanning, sending faxes and for photo-card access on
 most HP MFP (multi-function peripheral) printers.
 .
 This package contains data files for the HP Linux Printing and Imaging
 System.
Original-Maintainer: Debian Printing Team <debian-printing@lists.debian.org>

Package: hplip-doc
Source: hplip
Version: 3.21.4+dfsg0-1~peppermint1
Architecture: all
Maintainer: Steven Pusser <stevep@mxlinux.org>
Installed-Size: 792
Suggests: hplip
Multi-Arch: foreign
Homepage: https://developers.hp.com/hp-linux-imaging-and-printing
Priority: optional
Section: doc
Filename: pool/main/h/hplip/hplip-doc_3.21.4+dfsg0-1~peppermint1_all.deb
Size: 674408
SHA256: 88d0c12dc66787f408c97d3e906d4a38607ea05c9c1a158753cfe6d07d3fe383
SHA1: 653c53042ed939da0800dd6645e3a78121e91e9c
MD5sum: 9654be3cbff48d84191248599eb7bd94
Description: HP Linux Printing and Imaging - documentation
 The HP Linux Printing and Imaging System provides full support for
 printing on most HP SFP (single function peripheral) inkjets and many
 LaserJets, and for scanning, sending faxes and for photo-card access on
 most HP MFP (multi-function peripheral) printers.
 .
 This package contains the full documentation for the HP Linux
 Printing and Imaging System for off-line reading.  It includes
 the documentation for the HPIJS IJS driver as well.
Original-Maintainer: Debian Printing Team <debian-printing@lists.debian.org>

Package: hplip-gui
Source: hplip
Version: 3.21.4+dfsg0-1~peppermint1
Architecture: all
Maintainer: Steven Pusser <stevep@mxlinux.org>
Installed-Size: 146
Depends: default-dbus-session-bus | dbus-session-bus, hplip (>= 3.21.4+dfsg0-1~peppermint1), python3-dbus.mainloop.pyqt5, python3-pyqt5
Recommends: python3-notify2, xsane | simple-scan | skanlite
Homepage: https://developers.hp.com/hp-linux-imaging-and-printing
Priority: optional
Section: utils
Filename: pool/main/h/hplip/hplip-gui_3.21.4+dfsg0-1~peppermint1_all.deb
Size: 103636
SHA256: ebfa63b6c6adc3d6b6df2189b421e5e9a63370e1b054d58ce395037194b69eec
SHA1: 38d3e4fdedb058d58512872ab91e1ea687fcc9fb
MD5sum: 38260aab198545766b8178b5dfeaec56
Description: HP Linux Printing and Imaging - GUI utilities (Qt-based)
 The HP Linux Printing and Imaging System provides full support for
 printing on most HP SFP (single function peripheral) inkjets and many
 LaserJets, and for scanning, sending faxes and for photo-card access on
 most HP MFP (multi-function peripheral) printers.
 .
 This package contains utilities with graphical user interface (GUI) for
 HPLIP: HP Toolbox, HP Fax, ...
 .
 Note that all GUI utilities are based on the Qt GUI environment. There
 are currently no equivalent utilities based on GTK+.
Original-Maintainer: Debian Printing Team <debian-printing@lists.debian.org>

Package: ice
Version: 6.0.6
Architecture: all
Maintainer: Mark Greaves (PCNeSpec) <mark@pcnetspec.co.uk>
Installed-Size: 5306
Depends: python3, python3-gi, python3-requests, python3-bs4
Recommends: chromium-browser
Priority: optional
Section: net
Filename: pool/main/i/ice/ice_6.0.6_all.deb
Size: 45604
SHA256: b83a349470a243269e8620f55af231c37046f84c046e8985c8153b546fba3c86
SHA1: 1ac988baed33d9b929a7fc523ab49d9e07ee6fe5
MD5sum: 1e456352b981596a4c6597a56dabf990
Description: Webapp integration with Peppermint Ice
 Front end for building single site browsers using
 Chromium, Chrome, Vivaldi, or Firefox as a backend,
 allowing custom menu integration.

Package: nala
Version: 0.10.0
Architecture: all
Maintainer: Volian Developers <volian-devel@volian.org>
Installed-Size: 412
Depends: python3-anyio, python3-httpx, python3-pexpect, python3-rich, python3-tomli, python3-typer, python3-typing-extensions, python3:any, apt, python3-apt
Recommends: python3-socksio
Homepage: https://gitlab.com/volian/nala
Priority: optional
Section: admin
Filename: pool/main/n/nala/nala_0.10.0_all.deb
Size: 98908
SHA256: 8ebde769df7efc7fb1bb7a1d2af772bda21a57b200d0448c6e28564d405bc60f
SHA1: d3400d61becffdf284ef5810bed03e355cd084fa
MD5sum: 5a8c35ae73697aaee59ef3c688298073
Description: Commandline frontend for the APT package manager
 Nala is a frontend for the APT package manager. It has a lot
 of the same functionality, but formats the output to be more
 human readable. Also implements a history function to see past
 transactions and undo/redo them. Much like Fedora's dnf history.

Package: pepermint-wallpapers
Version: 03092022-1
Architecture: all
Maintainer: Manuel Rosa <manuelrosa@acoroslinux.tk>
Installed-Size: 11568
Priority: optional
Section: x11
Filename: pool/main/p/pepermint-wallpapers/pepermint-wallpapers_03092022-1_all.deb
Size: 11781076
SHA256: 1d75fe79d3ba0ea6b9e8ef4a9c2909582c66d8fad2e23a2901a7913e695a290f
SHA1: 9710e511ee36014e75791120d8b84e9d6f29dcb3
MD5sum: e45132aec682291b3fd72114b64c1348
Description: Wallpaper pack for Peppermint

Package: peppermint-gtk-themes
Version: 03092022-1
Architecture: all
Maintainer: Manuel Rosa <manuelrosa@acoroslinux.tk>
Installed-Size: 12493
Depends: arc-theme
Priority: optional
Section: x11
Filename: pool/main/p/peppermint-gtk-themes/peppermint-gtk-themes_03092022-1_all.deb
Size: 493564
SHA256: 37d354fe59d01ec09e9d4a70afeef1775af3b83881c3bbc11ecd36d6ef513a42
SHA1: 891467c01eaea2041e2cd9e7a30b18b231dfd034
MD5sum: 690d37101cdae306c22043b65e9e2b2e
Description: gtk themes set for Peppermint

Package: peppermint-icon-themes
Version: 03092022-1
Architecture: all
Maintainer: Manuel Rosa <manuelrosa@acoroslinux.tk>
Installed-Size: 109811
Priority: optional
Section: x11
Filename: pool/main/p/peppermint-icon-themes/peppermint-icon-themes_03092022-1_all.deb
Size: 7042704
SHA256: 8524bf2588325addfa9378e2c866fb5a90a8ba03fda73dd188c820d91a5603ed
SHA1: 890921babbf2e0dbf1edfe835486a4e17c2c7156
MD5sum: 256e8a4d2d76d007c4832ffef59955b7
Description: Numix and Pepirus icon themes for Peppermint

Package: peppermint-keyring
Version: 1.0-1peppermint1
Architecture: all
Maintainer: Manuel Rosa <manuelrosa@acoroslinux.pt>
Installed-Size: 15
Priority: optional
Section: system
Filename: pool/main/p/peppermint-keyring/peppermint-keyring_1.0-1peppermint1_all.deb
Size: 4108
SHA256: 94e594cd314f21b6d267cc2dbb5f3048ae65cb3941e49f4b43110abbbd06f598
SHA1: b3c7d70b85d96a22a15422ebb281e53656fb84dd
MD5sum: 939d9a763ac1c0ac0ef9d77fa11a55d3
Description: Peppermint keyring

Package: pepsources
Version: 1.0-1peppermint2
Architecture: all
Maintainer: Manuel Rosa <manuelrosa@acoroslinux.tk>
Installed-Size: 13
Homepage: https://peppermintos.com/
Priority: optional
Section: system
Filename: pool/main/p/pepsources/pepsources_1.0-1peppermint2_all.deb
Size: 1904
SHA256: b5faa5e7c4524c1d4b1f754194f7098b86426864d800241163b1d7c73bfb2138
SHA1: 5dd7e8dac8e70f5f22626cf2a280331f5ea50a63
MD5sum: 1a71a2ba78df1d0c1b29589fc0f3fbba
Description: Sources.list package for peppermint linux

Package: python3-anyio
Source: python-anyio
Version: 3.6.1-1
Architecture: all
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Installed-Size: 329
Depends: python3-idna, python3-sniffio, python3-typing-extensions | python3 (>> 3.8), python3:any
Homepage: https://github.com/agronholm/anyio
Priority: optional
Section: python
Filename: pool/main/p/python-anyio/python3-anyio_3.6.1-1_all.deb
Size: 53964
SHA256: 18005706c13de63807b506e861e4ceb893b0df508e1fe5f51e21bdf7beb62a72
SHA1: caba61ad28662bcaa681dd3e9314ff49312f1463
MD5sum: 29f2391c8d9109f30e08da479c0c8414
Description: Asynchronous compatibility layer (Python 3)
 This module provides a bridge to run the same sources unmodified
 on asyncio, curio and trio.
 .
 It bridges the following features:
  - task groups
  - cancellation
  - threads
  - signal handling
  - asynchronous file operations
  - subprocesses
  - inter-task synchronization and communication
  - high-level networking
 .
 This package installs the library for Python 3.

Package: python3-click
Source: python-click
Version: 8.0.3-1
Architecture: all
Maintainer: Sandro Tosi <morph@debian.org>
Installed-Size: 380
Depends: python3-colorama, python3-importlib-metadata | python3 (>> 3.8), python3:any
Breaks: python3-click-threading (<< 0.5.0)
Homepage: https://github.com/pallets/click
Priority: optional
Section: python
Filename: pool/main/p/python-click/python3-click_8.0.3-1_all.deb
Size: 91868
SHA256: f647cec49e0413756c2ece291b7413f6676d320ee1bbf3f252dbd2cc974740e4
SHA1: e5544a3ae6c586588c87a8af6a383413a9bb5848
MD5sum: 9bdb41a0ccd0234176950b2c76545ffd
Description: Wrapper around optparse for command line utilities - Python 3.x
 Click is a Python package for creating beautiful command line interfaces
 in a composable way with as little code as necessary.  It's the "Command
 Line Interface Creation Kit".  It's highly configurable but comes with
 sensible defaults out of the box.
 .
 It aims to make the process of writing command line tools quick and fun
 while also preventing any frustration caused by the inability to implement
 an intended CLI API.
 .
 This is the Python 3 compatible package.

Package: python3-colorama
Source: python-colorama
Version: 0.4.5-1
Architecture: all
Maintainer: Gürkan Myczko <tar@debian.org>
Installed-Size: 94
Depends: python3:any (>= 3.6~)
Homepage: https://github.com/tartley/colorama
Priority: optional
Section: python
Filename: pool/main/p/python-colorama/python3-colorama_0.4.5-1_all.deb
Size: 29288
SHA256: d050d8a45eda11d46e0d0e1d016a11b1335fa17c9850ae2f6ea542e1b0b8d66e
SHA1: 29eff85126d7734aa754b61c04dc2e81e9bd5882
MD5sum: 19fdae7d83744d96e5cb8a4fff5c788b
Description: Cross-platform colored terminal text in Python - Python 3.x
 Python-colorama provides a simple cross-platform API to print colored terminal
 text from Python applications.
 .
 ANSI escape character sequences are commonly used to produce colored terminal
 text on Unix. Colorama provides some shortcuts to generate these sequences.
 .
 This has the happy side-effect that existing applications or libraries which
 already use ANSI sequences to produce colored output on Linux.
 .
 This package provides the module for Python 3.

Package: python3-commonmark
Source: commonmark
Version: 0.9.1-4
Architecture: all
Maintainer: Sandro Tosi <morph@debian.org>
Installed-Size: 212
Depends: python3:any
Homepage: https://github.com/rtfd/commonmark.py
Priority: optional
Section: python
Filename: pool/main/c/commonmark/python3-commonmark_0.9.1-4_all.deb
Size: 45076
SHA256: d9806767a7197e14adeef1ec3d012790ab9fcbff23b72297a759e589c8a73439
SHA1: ae8075016c9f3f67a1eccd98b61c58c1a6cc1a2f
MD5sum: 15913cd60cc88c76d773239572eecfe2
Description: Python parser for the CommonMark Markdown spec
 commonmark.py is a pure Python port of jgm's commonmark.js, a Markdown parser
 and renderer for the CommonMark specification, using only native modules.

Package: python3-h11
Source: python-h11
Version: 0.13.0-2
Architecture: all
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Installed-Size: 235
Depends: python3-typing-extensions | python3 (>> 3.8), python3:any
Homepage: https://github.com/python-hyper/h11
Priority: optional
Section: python
Filename: pool/main/p/python-h11/python3-h11_0.13.0-2_all.deb
Size: 49872
SHA256: a1977ec6644120545c76095c7b19b0220747ee4107b12d057d05cd419140a9fb
SHA1: 8ed3603be73e98564d74730b260dfa5de319250c
MD5sum: 8e72771180a292f23f36801f889a12f0
Description: Pure-Python, bring-your-own-I/O implementation of HTTP/1.1 (Python 3)
 HTTP/1.1 library written from scratch in Python, heavily inspired by
 hyper-h2.
 .
 It's a "bring-your-own-I/O" library; h11 contains no IO code
 whatsoever. This means you can hook h11 up to your favorite network
 API, and that could be anything you want: synchronous, threaded,
 asynchronous, or your own implementation of RFC 6214 – h11 won’t
 judge you.
 .
 This is the Python 3 package.

Package: python3-httpcore
Source: httpcore
Version: 0.15.0-1
Architecture: all
Maintainer: Sandro Tosi <morph@debian.org>
Installed-Size: 276
Depends: python3-h11 (>= 0.11), python3-anyio, python3-certifi, python3-sniffio, python3:any
Homepage: https://github.com/encode/httpcore
Priority: optional
Section: python
Filename: pool/main/h/httpcore/python3-httpcore_0.15.0-1_all.deb
Size: 37148
SHA256: 74167867cacb847c75aa7cec5d4fbce5ae74a4d48d2a5cfffbcd724fd5030251
SHA1: b596c8225f3bc5f1c3a1f12e01c524a71a4ea30e
MD5sum: ac2ee4ec846b580734e28fe56ce4c6b4
Description: minimal low-level HTTP client
 The HTTP Core package provides a minimal low-level HTTP client, which does one
 thing only. Sending HTTP requests.
 .
 It does not provide any high level model abstractions over the API, does not
 handle redirects, multipart uploads, building authentication headers,
 transparent HTTP caching, URL parsing, session cookie handling, content or
 charset decoding, handling JSON, environment based configuration defaults, or
 any of that Jazz.
 .
 Some things HTTP Core does do:
 .
  * Sending HTTP requests.
  * Provides both sync and async interfaces.
  * Supports HTTP/1.1 and HTTP/2.
  * Async backend support for asyncio and trio.
  * Automatic connection pooling.
  * HTTP(S) proxy support.

Package: python3-httpx
Source: httpx
Version: 0.23.0-1
Architecture: all
Maintainer: Sandro Tosi <morph@debian.org>
Installed-Size: 379
Depends: python3-click (>= 8), python3-pygments, python3-rich (>= 10), python3-certifi, python3-httpcore (>= 0.15.0), python3-rfc3986 (>= 1.3.0), python3-sniffio, python3:any
Homepage: https://www.python-httpx.org/
Priority: optional
Section: python
Filename: pool/main/h/httpx/python3-httpx_0.23.0-1_all.deb
Size: 83332
SHA256: a8b275004277da3bca93d714ac3b4b6b166dd88068ccc38bfd8aecd9712838a4
SHA1: 1819b24d4a618a8d3dedcdbbcb0a1f89a57fd05e
MD5sum: ea4300beb8bf276c26999a50cdb47e20
Description: next generation HTTP client
 HTTPX is a fully featured HTTP client for Python 3, which provides sync and
 async APIs, and support for both HTTP/1.1 and HTTP/2.
 .
 HTTPX is a high performance asynchronous HTTP client, that builds on the
 well-established usability of requests, and gives you:
 .
  * A broadly requests-compatible API.
  * Standard synchronous interface, but with async support if you need it.
  * HTTP/1.1 and HTTP/2 support.
  * Ability to make requests directly to WSGI applications or ASGI applications.
  * Strict timeouts everywhere.
  * Fully type annotated.
  * 99% test coverage.
 .
 Plus all the standard features of requests:
 .
  * International Domains and URLs
  * Keep-Alive & Connection Pooling
  * Sessions with Cookie Persistence
  * Browser-style SSL Verification
  * Basic/Digest Authentication
  * Elegant Key/Value Cookies
  * Automatic Decompression
  * Automatic Content Decoding
  * Unicode Response Bodies
  * Multipart File Uploads
  * HTTP(S) Proxy Support
  * Connection Timeouts
  * Streaming Downloads
  * .netrc Support
  * Chunked Requests

Package: python3-pexpect
Source: pexpect
Version: 4.8.0-3
Architecture: all
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Installed-Size: 208
Depends: python3-ptyprocess, python3:any
Suggests: python-pexpect-doc
Homepage: https://github.com/pexpect/pexpect
Priority: optional
Section: python
Filename: pool/main/p/pexpect/python3-pexpect_4.8.0-3_all.deb
Size: 55048
SHA256: 5a56a85f63d29a25ff6ccc5b7c2dfd38715038dc581991cad60b14fa4cd05e88
SHA1: 7e20314eb8635f302c768a10b683059404dde9fc
MD5sum: cda3894198cbc63be0dc10304ccbff08
Description: Python 3 module for automating interactive applications
 Pexpect is a pure Python 3 module for spawning child applications;
 controlling them; and responding to expected patterns in their
 output. Pexpect works like Don Libes' Expect. Pexpect allows your
 script to spawn a child application and control it as if a human were
 typing commands.

Package: python3-ptyprocess
Source: ptyprocess
Version: 0.7.0-3
Architecture: all
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Installed-Size: 59
Depends: python3:any
Homepage: https://github.com/pexpect/ptyprocess
Priority: optional
Section: python
Filename: pool/main/p/ptyprocess/python3-ptyprocess_0.7.0-3_all.deb
Size: 14888
SHA256: 58cded4e665a078d6d4d4b0885da93c2da18b64777ead449245f3e5bb485073a
SHA1: ec39916e6fa4ef0bef434db6c4043b4b34f1c689
MD5sum: 5634f13df1bf4435ac20e8ad2f4d6042
Description: Run a subprocess in a pseudo terminal from Python 3
 Launch a subprocess in a pseudo terminal (pty), and interact with both
 the process and its pty.
 .
 Sometimes, piping stdin and stdout is not enough. There might be a password
 prompt that doesn't read from stdin, output that changes when it's going to
 a pipe rather than a terminal, or curses-style interfaces that rely on a
 terminal. If you need to automate these things, running the process in a
 pseudo terminal (pty) is the answer.
 .
 This package installs the library for Python 3.

Package: python3-pygments
Source: pygments
Version: 2.12.0+dfsg-2
Architecture: all
Maintainer: Piotr Ożarowski <piotr@debian.org>
Installed-Size: 4130
Depends: python3-pkg-resources, python3:any
Suggests: python-pygments-doc, ttf-bitstream-vera
Breaks: python-pygments (<< 2.3.1+dfsg-4~)
Replaces: python-pygments (<< 2.3.1+dfsg-4~)
Multi-Arch: foreign
Homepage: https://pygments.org/
Priority: optional
Section: python
Filename: pool/main/p/pygments/python3-pygments_2.12.0+dfsg-2_all.deb
Size: 764804
SHA256: b50749d9328d2c1481625123e95139aad1ae695b03fd5f10b5fcf028c5aaf7fc
SHA1: bcb0b6687a37e280cee49252879b38258bd6d229
MD5sum: 0fe86fb2df18208426aa3afecf31ed97
Description: syntax highlighting package written in Python 3
 Pygments aims to be a generic syntax highlighter for general use in all kinds
 of software such as forum systems, wikis or other applications that need to
 prettify source code.
 .
 Highlights are:
   * a wide range of common languages and markup formats is supported
   * special attention is paid to details, increasing quality by a fair amount
   * support for new languages and formats are added easily
   * a number of output formats, presently HTML, LaTeX and ANSI sequences
   * it is usable as a command-line tool and as a library

Package: python3-rfc3986
Source: python-rfc3986
Version: 1.5.0-2
Architecture: all
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Installed-Size: 125
Depends: python3-idna, python3:any
Homepage: https://rfc3986.rtfd.org
Priority: optional
Section: python
Filename: pool/main/p/python-rfc3986/python3-rfc3986_1.5.0-2_all.deb
Size: 22184
SHA256: 45f369de2e8f55b8b28a4a4a2e0f7283a98218a9319cb3cc3114865513b6e7ef
SHA1: 9aacc65980f3f418ef1fe201f81563d9af8d5ace
MD5sum: 27fb91c43fe51cbb20c57322291f3fa8
Description: validating URI references per RFC 3986 - Python 3.x
 This package provides a Python implementation of RFC 3986, including
 validation and authority parsing.
 .
 This package contains the Python 3.x module.

Package: python3-rich
Source: rich
Version: 12.4.4-1
Architecture: all
Maintainer: Sandro Tosi <morph@debian.org>
Installed-Size: 1027
Depends: python3-commonmark (>= 0.9.0), python3-pygments (>= 2.6.0), python3-typing-extensions (>= 3.7.4) | python3 (>> 3.9), python3:any
Homepage: https://github.com/willmcgugan/rich
Priority: optional
Section: python
Filename: pool/main/r/rich/python3-rich_12.4.4-1_all.deb
Size: 194608
SHA256: a5302f53cbde406625e90abaf7f669b22f216fd4bb95fe0c4a1fc6f874c488a5
SHA1: 7ca88146ad81ecef31dfeec903c6857f0c3b992f
MD5sum: d772add2e8855c74a02160d2e0b75d67
Description: render rich text, tables, progress bars, syntax highlighting, markdown and more
 Rich is a Python library for rich text and beautiful formatting in the
 terminal.
 .
 The Rich API makes it easy to add color and style to terminal output. Rich can
 also render pretty tables, progress bars, markdown, syntax highlighted source
 code, tracebacks, and more — out of the box.
 .
 Here's a list of the core functionalities of rich:
 .
  * to effortlessly add rich output to your application, you can import the rich
    print method, which has the same signature as the builtin Python function
  * Rich can be installed in the Python REPL, so that any data structures will
    be pretty printed and highlighted
  * for more control over rich terminal content, import and construct a Console
    object. The Console object has a print method which has an intentionally
    similar interface to the builtin print function
  * to insert an emoji in to console output place the name between two colons
  * Rich can render flexible tables with unicode box characters. There is a
    large variety of formatting options for borders, styles, cell alignment etc
  * Rich can render multiple flicker-free progress bars to track long-running
    tasks.
  * Rich can render content in neat columns with equal or optimal width.
  * Rich can render markdown and does a reasonable job of translating the
    formatting to the terminal
  * Rich can render beautiful tracebacks which are easier to read and show more
    code than standard Python tracebacks. You can set Rich as the default
    traceback handler so all uncaught exceptions will be rendered by Rich.

Package: python3-sniffio
Source: python-sniffio
Version: 1.2.0-1
Architecture: all
Maintainer: Robie Basak <robie@justgohome.co.uk>
Installed-Size: 34
Depends: python3:any (>= 3.7~)
Homepage: https://github.com/python-trio/sniffio
Priority: optional
Section: python
Filename: pool/main/p/python-sniffio/python3-sniffio_1.2.0-1_all.deb
Size: 6372
SHA256: d676bccd211f3e521154a12fb9a8fd588624633bf529039e5f27027f40e75cde
SHA1: 872b0a2c472938f47a5a285e5d8b77e66ef009f4
MD5sum: 664292fc216e237ce689feacd65dde71
Description: detect which async Python library is in use
 Python libraries that support multiple async packages (like Trio, asyncio,
 etc) need to know which is in use. This library provides this information.

Package: python3-socksio
Source: socksio
Version: 1.0.0-2
Architecture: all
Maintainer: Sandro Tosi <morph@debian.org>
Installed-Size: 66
Depends: python3:any
Homepage: https://github.com/sethmlarson/socksio
Priority: optional
Section: python
Filename: pool/main/s/socksio/python3-socksio_1.0.0-2_all.deb
Size: 14868
SHA256: 277912e48ec244619e1e47bd75945d8e3108bf6a8c23458c9128e126a70b6b0a
SHA1: 502dcf69a5d20412aa0b0b965584d5b8e28f29f2
MD5sum: 81712df34325f459bce7642e548255be
Description: Sans-I/O implementation of SOCKS4, SOCKS4A, and SOCKS5
 Client-side sans-I/O SOCKS proxy implementation. Supports SOCKS4, SOCKS4A, and
 SOCKS5.
 .
 socksio is a sans-I/O library similar to h11 or h2, this means the library
 itself does not handle the actual sending of the bytes through the network, it
 only deals with the implementation details of the SOCKS protocols so you can
 use it in any I/O library you want.

Package: python3-tomli
Source: python-tomli
Version: 2.0.1-1
Architecture: all
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Installed-Size: 61
Depends: python3:any
Homepage: https://github.com/hukkin/tomli
Priority: optional
Section: python
Filename: pool/main/p/python-tomli/python3-tomli_2.0.1-1_all.deb
Size: 18104
SHA256: 7111172bd9c66ad67be517f3e4f121cfc260f4b0674fc27c502129fc120ac44e
SHA1: 95d58aa0a055df207463f2912bd2b5beb9045fd2
MD5sum: 40d410a996faea738aacb9eaa8718c61
Description: lil' TOML parser for Python
 Tomli is a Python library for parsing TOML. https://toml.io/
 Tomli is fully compatible with TOML v1.0.0. https://toml.io/en/v1.0.0

Package: python3-typer
Source: typer
Version: 0.4.1-1
Architecture: all
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Installed-Size: 129
Depends: python3-click, python3:any
Suggests: python-typer-doc
Homepage: https://typer.tiangolo.com/
Priority: optional
Section: python
Filename: pool/main/t/typer/python3-typer_0.4.1-1_all.deb
Size: 23628
SHA256: f5bd46e4cd1a0006ee675c1b76bb00415eace2e7a0ec38a322a6e9a8d0e8c701
SHA1: 537aa5e1097fb795057b45ef13a8a933f4048d28
MD5sum: 14d764fce2c554a93a943388b1c3c7f0
Description: Python library for building CLI applications (Python 3)
 Typer is a library for building CLI applications that users
 will love using and developers will love creating. Based on
 Python 3.6+ type hints.
 .
 The key features are:
 .
 Intuitive to write: Great editor support. Completion everywhere.
 Less time debugging. Designed to be easy to use and learn. Less
 time reading docs.
 .
 Easy to use: It's easy to use for the final users. Automatic help,
 and automatic completion for all shells.
 .
 Short: Minimize code duplication. Multiple features from each
 parameter declaration. Fewer bugs.
 .
 Start simple: The simplest example adds only 2 lines of code to your
 app: 1 import, 1 function call.
 .
 Grow large: Grow in complexity as much as you want, create arbitrarily
 complex trees of commands and groups of subcommands, with options and
 arguments.
 .
 This package installs the library for Python 3.

Package: python3-typing-extensions
Source: python-typing-extensions
Version: 3.10.0.2-1
Architecture: all
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Installed-Size: 213
Depends: python3:any
Homepage: https://github.com/python/typing/blob/master/typing_extensions/README.rst
Priority: optional
Section: python
Filename: pool/main/p/python-typing-extensions/python3-typing-extensions_3.10.0.2-1_all.deb
Size: 35184
SHA256: 004ef709d5573550d5752d608735adbd08eaf7a9f4b6eb7bd859d873f2f169b8
SHA1: e023b77d8be1fff6da70235927e12a8c483c6a62
MD5sum: 27c0718df352e63a7bc701e8f2946e7a
Description: Backported and Experimental Type Hints for Python
 The typing module was added to the standard library in Python 3.5 on a
 provisional basis and will no longer be provisional in Python 3.7. However,
 this means users of Python 3.5 - 3.6 who are unable to upgrade will not be
 able to take advantage of new types added to the typing module, such as
 typing.Text or typing.Coroutine.
 .
 The typing_extensions module contains both backports of these changes as well
 as experimental types that will eventually be added to the typing module, such
 as Protocol.
 .
 Users of other Python versions should continue to install and use the typing
 module from PyPI instead of using this one unless specifically writing code
 that must be compatible with multiple Python versions or requires experimental
 types.

Package: rtl8812au-dkms
Source: rtl8812au
Version: 5.13.6+git20220728-0~peppermint1
Architecture: all
Maintainer: Timothy E. Harris <maintainer@mxrepo.com>
Installed-Size: 15141
Depends: dkms
Homepage: https://github.com/morrownr/8812au-20210629
Priority: optional
Section: misc
Filename: pool/main/r/rtl8812au/rtl8812au-dkms_5.13.6+git20220728-0~peppermint1_all.deb
Size: 1869468
SHA256: 587edc4a518d776ecbecfd19dcbd03a4b39aff5e94e7f175c20c0926f33f8246
SHA1: 75be65d92c269d6d5007a5be7e8fee485fdb9d25
MD5sum: 0a4cfc263add4a62f0c4c6ad26345d84
Description: rtl8812au driver in DKMS format.
 The rtl8812au driver provides the driver for Realtek 802.11ac (rtl8812au).
 This package contains the DKMS files to build and install rtl8812au.

Package: rtl8814au-dkms
Source: rtl8814au
Version: 5.8.5.1-1~peppermint1
Architecture: all
Maintainer: SwampRabbit <swamprabbit@mxlinux.org>
Installed-Size: 13226
Depends: debconf (>= 0.5) | debconf-2.0, dkms (>= 2.1.0.0), linux-libc-dev, libc6-dev
Homepage: http://www.realtek.com.tw/
Priority: optional
Section: kernel
Filename: pool/main/r/rtl8814au/rtl8814au-dkms_5.8.5.1-1~peppermint1_all.deb
Size: 1601924
SHA256: dbcd4cb0836b2b17fe59082dc719509bc9072ef49ca23cce8b19b15844c61aee
SHA1: 81433c4df6d177ce7dd49e2d57d8a11b5378be72
MD5sum: 71c43b7ea0c0da9efdad98009c890bd0
Description: dkms source for the rtl8814au network driver
 rtl8814au is the Linux device driver released for the RealTek RTL8814AU WiFi
 controllers with USB interface.
 .
 This package provides the dkms source code for the rtl8814au kernel module.
 Kernel source or headers are required to compile these modules.
Original-Maintainer: jimmy <jimmy@jimmy-alpha>

Package: rtl8821ce-dkms
Source: rtl8821ce
Version: 5.5.2.1-7~peppermint1
Architecture: all
Maintainer: Steven Pusser <stevep@mxlinux.org>
Installed-Size: 24644
Depends: bc, dkms (>= 2.1.0.0)
Homepage: https://launchpad.net/ubuntu/+source/rtl8821ce
Priority: optional
Section: kernel
Filename: pool/main/r/rtl8821ce/rtl8821ce-dkms_5.5.2.1-7~peppermint1_all.deb
Size: 2197796
SHA256: 99bd79d9b5378f116a62fc6b80dbb9f712df1bb85cb1a8ec6bc01d24751a8678
SHA1: e0be492a8c21d2b1e61957a10c49820814cb78d8
MD5sum: 64b674a78a17182b6119b183da049331
Description: DKMS source for the Realtek 8821C PCIe Wi-Fi driver
 Realtek provides the solution for WLAN in hardware and software. The software
 package could be adopted for Realtek RTL8821CE WLAN series hardware on Linux
 based platforms.
 .
 This package provides the DKMS source code for the rtl8821ce kernel module.
 Kernel source or headers are required to compile these modules.
Modaliases: rtl8821ce(pci:v000010ECd0000C821sv*sd*bc*sc*i*, pci:v000010ECd0000C82Asv*sd*bc*sc*i*, pci:v000010ECd0000C82Bsv*sd*bc*sc*i*)
Original-Maintainer: Canonical HWE Team <canonical-hwe@lists.canonical.com>

Package: rtl8821cu-dkms
Version: 5.4.1-1~peppermint1
Architecture: all
Maintainer: SwampRabbit <swamprabbit@mxlinux.org>
Installed-Size: 24150
Provides: rtl8821cu-module
Multi-Arch: foreign
Priority: optional
Section: misc
Filename: pool/main/r/rtl8821cu-dkms/rtl8821cu-dkms_5.4.1-1~peppermint1_all.deb
Size: 2136952
SHA256: a75f86315f911ccb05cfa70d1dd13b148ccdddcd002f7adc0d16c1d21e35b5fe
SHA1: cd184728060654e87015096ec748a9069390b963
MD5sum: 65eac0a9b6860295fc6b0c485bb028da
Description: DKMS files to build and install rtl8821cu
 The rtl8821cu driver provides the driver for Realtek RTL8811CU/RTL8821CU
 USB wifi adapter.
 .
 This package contains the DKMS files to build and install rtl8821cu.
 .
 To install this package, you have to install the header files for your
 current kernel manually (usually included in the "linux-headers-<your
 architecture>").
Original-Maintainer: Marc Leeman <m.leeman@televic.com>

Package: rtl8821cu-src
Source: rtl8821cu-dkms
Version: 5.4.1-1~peppermint1
Architecture: all
Maintainer: SwampRabbit <swamprabbit@mxlinux.org>
Installed-Size: 2039
Depends: module-assistant, debhelper (>= 5.0.37), xz-utils
Priority: optional
Section: misc
Filename: pool/main/r/rtl8821cu-dkms/rtl8821cu-src_5.4.1-1~peppermint1_all.deb
Size: 2081096
SHA256: c3b45239cc864c84bb9eb39f2254a7f08c900b879e563fcb7ee2aa9b6e6575ac
SHA1: 027b4ed6a778fb46830702b645386dc6528c0195
MD5sum: 9cd5b4881ce890b5483eb6572f6067c6
Description: Source of the compressed rtl8821cu module
 The rtl8821cu driver provides the driver for Realtek RTL8811CU/RTL8821CU
 USB wifi adapter.
 .
 You can use module-assistant or make-kpkg to build a modules package
 for specific kernel.
Original-Maintainer: Marc Leeman <m.leeman@televic.com>

Package: sambashare
Version: 0.8.1
Architecture: all
Maintainer: Arjen Balfoort <arjenbalfoort@solydxk.com>
Installed-Size: 290
Depends: python3:any (>= 3.3.2-2~), python3, python3-gi, gir1.2-gtk-3.0, samba
Conflicts: solydxk-locale
Replaces: solydxk-locale
Priority: optional
Section: admin
Filename: pool/main/s/sambashare/sambashare_0.8.1_all.deb
Size: 21766
SHA256: 06a670e31514d471d9b1cdefa1e1f84fd6f7c1f0bbcc8ae2dd235edeb22764a0
SHA1: ce3ee699fe99fc3f8c297a749f354903a8f07b23
MD5sum: bea88ce4df31db9d5764c1e58a1a3881
Description: Simple samba share manager

