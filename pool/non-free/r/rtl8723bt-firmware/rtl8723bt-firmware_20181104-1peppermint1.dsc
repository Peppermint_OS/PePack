Format: 3.0 (quilt)
Source: rtl8723bt-firmware
Binary: firmware-realtek-rtl8723cs-bt
Architecture: all
Version: 20181104-1peppermint1
Maintainer: Bastian Germann <bage@debian.org>
Homepage: https://github.com/anarsoul/rtl8723bt-firmware
Standards-Version: 4.6.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 firmware-realtek-rtl8723cs-bt deb non-free/kernel optional arch=all
Checksums-Sha1:
 213216f0fd65647a64efb446b337f006af3ab8e6 18505 rtl8723bt-firmware_20181104.orig.tar.gz
 ac4f8faccdb56730d68dbf93b362597d2334dcae 2412 rtl8723bt-firmware_20181104-1peppermint1.debian.tar.xz
Checksums-Sha256:
 8fb44856ab878b206d3422358590b2bc3dce7b34b40202fd85d46888cd968a6f 18505 rtl8723bt-firmware_20181104.orig.tar.gz
 9c6c35ba60022b7fb7666592f85d0753f7e36f2070d81ece012e775479cab0b5 2412 rtl8723bt-firmware_20181104-1peppermint1.debian.tar.xz
Files:
 5bd3639731ca1c38484672561fa53761 18505 rtl8723bt-firmware_20181104.orig.tar.gz
 36608ed3189b4eabcff161ddb0b454ae 2412 rtl8723bt-firmware_20181104-1peppermint1.debian.tar.xz
Autobuild: yes
