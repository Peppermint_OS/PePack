Format: 3.0 (quilt)
Source: intel-media-driver-non-free
Binary: intel-media-va-driver-non-free
Architecture: amd64 i386 x32
Version: 21.4.1+ds1-1~peppermint1
Maintainer: SwampRabbit <swamprabbit@mxlinux.org>
Uploaders:  Sebastian Ramacher <sramacher@debian.org>
Homepage: https://github.com/intel/media-driver
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/multimedia-team/intel-media-driver-non-free
Vcs-Git: https://salsa.debian.org/multimedia-team/intel-media-driver-non-free.git
Build-Depends: debhelper-compat (= 13), dh-sequence-libva, cmake, libigdgmm-dev (>= 21.1.1), libva-dev (>= 2.12), libx11-dev, pkg-config
Package-List:
 intel-media-va-driver-non-free deb non-free/video optional arch=amd64,i386,x32
Checksums-Sha1:
 f2dc176c31afae36545e137c98844986921f56aa 12877092 intel-media-driver-non-free_21.4.1+ds1.orig.tar.xz
 5982deda2798b31960d2319883e912056c3aa456 5436 intel-media-driver-non-free_21.4.1+ds1-1~peppermint1.debian.tar.xz
Checksums-Sha256:
 37795d1148b8c0218c5acb30c6e3f46b07a7f350f3f5e53ca5b42940d6b951b0 12877092 intel-media-driver-non-free_21.4.1+ds1.orig.tar.xz
 cb90b9bacc23f24cbc4281f9fcd888c71ff4efc55a48c7655147ea8c0e7d0d42 5436 intel-media-driver-non-free_21.4.1+ds1-1~peppermint1.debian.tar.xz
Files:
 ec60f26e5bc37d0866c176bf5967a18e 12877092 intel-media-driver-non-free_21.4.1+ds1.orig.tar.xz
 a59a8c7ff291da1f13a17752e871808d 5436 intel-media-driver-non-free_21.4.1+ds1-1~peppermint1.debian.tar.xz
Autobuild: yes
Original-Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
