Format: 3.0 (quilt)
Source: firmware-sof
Binary: firmware-sof-signed
Architecture: all
Version: 2.0-1~peppermint1
Maintainer: Steven Pusser <stevep@mxlinux.org>
Homepage: https://github.com/thesofproject/sof-bin
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/mpearson/firmware-sof
Vcs-Git: https://salsa.debian.org/mpearson/firmware-sof.git
Build-Depends: debhelper-compat (= 12)
Package-List:
 firmware-sof-signed deb non-free/kernel optional arch=all
Checksums-Sha1:
 ce74e0072541ad70c5d928f36735c95250131761 26977577 firmware-sof_2.0.orig.tar.gz
 7fc07a62c6a84af988e965cf15dfc396d7b6b95b 9532 firmware-sof_2.0-1~peppermint1.debian.tar.xz
Checksums-Sha256:
 324a8ef46cc75797c4f17cad3563fd56a7ebfe0fb127939ed498c744c1061a98 26977577 firmware-sof_2.0.orig.tar.gz
 2f25f0bd8baa9e04228459fae5f9fd7159c350f92c4cc72075175b9db0445ae5 9532 firmware-sof_2.0-1~peppermint1.debian.tar.xz
Files:
 f19e6c5a73a3806cb06b003fde0b388c 26977577 firmware-sof_2.0.orig.tar.gz
 4a113be4b78d06b3170b478122cd4af2 9532 firmware-sof_2.0-1~peppermint1.debian.tar.xz
Autobuild: yes
Original-Maintainer: Mark Pearson <markpearson@lenovo.com>
