Format: 3.0 (quilt)
Source: firmware-nonfree
Binary: firmware-linux, firmware-linux-nonfree, firmware-qcom-media, firmware-amd-graphics, firmware-atheros, firmware-bnx2, firmware-bnx2x, firmware-brcm80211, firmware-cavium, firmware-intelwimax, firmware-intel-sound, firmware-ipw2x00, firmware-ivtv, firmware-iwlwifi, firmware-libertas, firmware-misc-nonfree, firmware-myricom, firmware-netronome, firmware-netxen, firmware-qcom-soc, firmware-qlogic, firmware-realtek, firmware-samsung, firmware-siano, firmware-ti-connectivity
Architecture: all
Version: 20210818-1~peppermint1
Maintainer: Debian Kernel Team <debian-kernel@lists.debian.org>
Uploaders: Bastian Blank <waldi@debian.org>, Steve Langasek <vorlon@debian.org>, maximilian attems <maks@debian.org>, Ben Hutchings <ben@decadent.org.uk>
Standards-Version: 4.0.1
Vcs-Browser: https://salsa.debian.org/kernel-team/firmware-nonfree
Vcs-Git: https://salsa.debian.org/kernel-team/firmware-nonfree.git
Build-Depends: debhelper (>= 9)
Package-List:
 firmware-amd-graphics deb non-free/kernel optional arch=all
 firmware-atheros deb non-free/kernel optional arch=all
 firmware-bnx2 deb non-free/kernel optional arch=all
 firmware-bnx2x deb non-free/kernel optional arch=all
 firmware-brcm80211 deb non-free/kernel optional arch=all
 firmware-cavium deb non-free/kernel optional arch=all
 firmware-intel-sound deb non-free/kernel optional arch=all
 firmware-intelwimax deb non-free/kernel optional arch=all
 firmware-ipw2x00 deb non-free/kernel optional arch=all
 firmware-ivtv deb non-free/kernel optional arch=all
 firmware-iwlwifi deb non-free/kernel optional arch=all
 firmware-libertas deb non-free/kernel optional arch=all
 firmware-linux deb non-free/metapackages optional arch=all
 firmware-linux-nonfree deb non-free/metapackages optional arch=all
 firmware-misc-nonfree deb non-free/kernel optional arch=all
 firmware-myricom deb non-free/kernel optional arch=all
 firmware-netronome deb non-free/kernel optional arch=all
 firmware-netxen deb non-free/kernel optional arch=all
 firmware-qcom-media deb non-free/oldlibs optional arch=all
 firmware-qcom-soc deb non-free/kernel optional arch=all
 firmware-qlogic deb non-free/kernel optional arch=all
 firmware-realtek deb non-free/kernel optional arch=all
 firmware-samsung deb non-free/kernel optional arch=all
 firmware-siano deb non-free/kernel optional arch=all
 firmware-ti-connectivity deb non-free/kernel optional arch=all
Checksums-Sha1:
 85f5e5be04c3cab1a51f112154efd9e07bdbb6d2 129015316 firmware-nonfree_20210818.orig.tar.xz
 6b6f3fa446b5d79245d4551b85ec3a86a0b3ce70 821832 firmware-nonfree_20210818-1~peppermint1.debian.tar.xz
Checksums-Sha256:
 fccbdcf7c015d9dc7cbb01c90ee58d7b88b7460b2f29316987402715f82590a9 129015316 firmware-nonfree_20210818.orig.tar.xz
 9f4ab1f600c1a028cd0eb79f7b33a4d74c3c7a22dd83f8160ab35081d8f07d50 821832 firmware-nonfree_20210818-1~peppermint1.debian.tar.xz
Files:
 79999b85a2be0b02479dd634df94cfdf 129015316 firmware-nonfree_20210818.orig.tar.xz
 7a46c50acd760fa9e2d521d54dd6303e 821832 firmware-nonfree_20210818-1~peppermint1.debian.tar.xz
Autobuild: yes
