Format: 3.0 (quilt)
Source: broadcom-sta
Binary: broadcom-sta-common, broadcom-sta-dkms, broadcom-sta-source
Architecture: all
Version: 6.30.223.271-20peppermint1
Maintainer: Eduard Bloch <blade@debian.org>
Uploaders: Cyril Lacoux <clacoux@easter-eggs.com>, Roger Shimizu <rosh@debian.org>
Homepage: http://www.broadcom.com/support/802.11/linux_sta.php
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/broadcom-sta-team/broadcom-sta
Vcs-Git: https://salsa.debian.org/broadcom-sta-team/broadcom-sta.git
Build-Depends: debhelper-compat (= 12), dkms
Package-List:
 broadcom-sta-common deb non-free/kernel optional arch=all
 broadcom-sta-dkms deb non-free/kernel optional arch=all
 broadcom-sta-source deb non-free/kernel optional arch=all
Checksums-Sha1:
 38f040920b064b244d47bd560a669308fd10cac8 2194116 broadcom-sta_6.30.223.271.orig.tar.xz
 b9b224134d1c79e15bbb4b0c342724e898cc02d9 29820 broadcom-sta_6.30.223.271-20peppermint1.debian.tar.xz
Checksums-Sha256:
 8b539f173ab7092d34307b6306e22c6380df025a7f2483937374492a7aedb2dc 2194116 broadcom-sta_6.30.223.271.orig.tar.xz
 239c725c052ed712678a9dea37ec0d42a27fd97054a7ab6f0aa595f36150491b 29820 broadcom-sta_6.30.223.271-20peppermint1.debian.tar.xz
Files:
 f2d9a16d5c36afa112ea9cef2f3592be 2194116 broadcom-sta_6.30.223.271.orig.tar.xz
 ef1568b83ec46f5740efa2b9f5575d88 29820 broadcom-sta_6.30.223.271-20peppermint1.debian.tar.xz
Autobuild: yes
