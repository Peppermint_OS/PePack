Format: 3.0 (quilt)
Source: pep-lxde-settings
Binary: pep-lxde-settings
Architecture: all
Version: 1.0-1peppermint1
Maintainer: Manuel Rosa <manuelsilvarosa@gmail.com>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 pep-lxde-settings deb x11 optional arch=all
Checksums-Sha1:
 28e0d4e88889b809b7bcda02afe47404558fd3a7 13296 pep-lxde-settings_1.0.orig.tar.xz
 881b4468470e25c764ddff9a61c6014b343eee16 1512 pep-lxde-settings_1.0-1peppermint1.debian.tar.xz
Checksums-Sha256:
 92602028de729c1a4e310f4a186c4025544abbddcb36a1bcd33d89f9bc45dfbb 13296 pep-lxde-settings_1.0.orig.tar.xz
 53a453f88907508522a1c3b72644f7d7f3844cc89aec33b851066b01c9d1c707 1512 pep-lxde-settings_1.0-1peppermint1.debian.tar.xz
Files:
 66226ff4bedc7904d6a6d65717724b8b 13296 pep-lxde-settings_1.0.orig.tar.xz
 297a9e771ef5423a6e346aa6bb6a2fe8 1512 pep-lxde-settings_1.0-1peppermint1.debian.tar.xz
