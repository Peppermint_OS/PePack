Format: 3.0 (quilt)
Source: peppermint-gtk-themes
Binary: peppermint-gtk-themes
Architecture: all
Version: 03092022-1
Maintainer: Manuel Rosa <manuelrosa@acoroslinux.tk>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 peppermint-gtk-themes deb x11 optional arch=all
Checksums-Sha1:
 6e547bfa59d3a9e1a05d4e1d6430cc20afdf1ac9 473080 peppermint-gtk-themes_03092022.orig.tar.xz
 1d6c623b1231134c108f0e74c49efed996bcc8ef 1184 peppermint-gtk-themes_03092022-1.debian.tar.xz
Checksums-Sha256:
 164bcf411745e500bd393ffd082a9ded8d8a9a4e8c54b6742b710e710f137f60 473080 peppermint-gtk-themes_03092022.orig.tar.xz
 6d7896333800c077370ecc359a9e6e9556cc0bf9e7b8f7d224077f0b3c52ca69 1184 peppermint-gtk-themes_03092022-1.debian.tar.xz
Files:
 71734d351747e057712e06485ec62b5f 473080 peppermint-gtk-themes_03092022.orig.tar.xz
 d119eabca862b1ac89e0a479529212ba 1184 peppermint-gtk-themes_03092022-1.debian.tar.xz
