Format: 3.0 (quilt)
Source: pepermint-wallpapers
Binary: pepermint-wallpapers
Architecture: all
Version: 15042023-2peppermint4
Maintainer: Manuel Rosa <manuelrosa@acoroslinux.tk>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 pepermint-wallpapers deb x11 optional arch=all
Checksums-Sha1:
 9065bc882561f91b45b31be3ebdb3dbc47608fb5 3010884 pepermint-wallpapers_15042023.orig.tar.xz
 b257db8c3ad8198212105886b569c33e8464e83c 1396 pepermint-wallpapers_15042023-2peppermint4.debian.tar.xz
Checksums-Sha256:
 cb1e3032c65b43e59ab69b2df414581820e6ad93f3c43db77c4f42d94baa1ff9 3010884 pepermint-wallpapers_15042023.orig.tar.xz
 51f60a68f42a607fe9fb8f51395ebc4833aa5b5dff57035072c8cc8efc60aed8 1396 pepermint-wallpapers_15042023-2peppermint4.debian.tar.xz
Files:
 dc5355c46ca99e097fa2ce03560dc497 3010884 pepermint-wallpapers_15042023.orig.tar.xz
 708dd09e80cd9f94db45dec3386917c6 1396 pepermint-wallpapers_15042023-2peppermint4.debian.tar.xz
