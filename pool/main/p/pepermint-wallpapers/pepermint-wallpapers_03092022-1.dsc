Format: 3.0 (quilt)
Source: pepermint-wallpapers
Binary: pepermint-wallpapers
Architecture: all
Version: 03092022-1
Maintainer: Manuel Rosa <manuelrosa@acoroslinux.tk>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 pepermint-wallpapers deb x11 optional arch=all
Checksums-Sha1:
 719692c7a502cb0c32c44fa2fabbfc23e8511c05 11782732 pepermint-wallpapers_03092022.orig.tar.xz
 a4ba1bae5b73c67c785534303000db48a445b728 1196 pepermint-wallpapers_03092022-1.debian.tar.xz
Checksums-Sha256:
 402725fefb9f657d84a69a0b8e550dc7805b7d837242f45e7a034d04fd0172cc 11782732 pepermint-wallpapers_03092022.orig.tar.xz
 caa24fb5598e70ea3c81056db7aceb1f5d2bf477a4afcce9e87fbb124dab0ac5 1196 pepermint-wallpapers_03092022-1.debian.tar.xz
Files:
 d08be83e1f41f98893b9e05ed977de78 11782732 pepermint-wallpapers_03092022.orig.tar.xz
 811369262909600a2caf2cd6ee116362 1196 pepermint-wallpapers_03092022-1.debian.tar.xz
