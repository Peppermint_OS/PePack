Format: 3.0 (quilt)
Source: pep-kde-settings
Binary: pep-kde-settings
Architecture: all
Version: 1.0-1peppermint1
Maintainer: Manuel Rosa <manuelsilvarosa@gmail.com>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 pep-kde-settings deb x11 optional arch=all
Checksums-Sha1:
 f2421d7ab20376edad3295b296d0c9f474fca220 12192 pep-kde-settings_1.0.orig.tar.xz
 9cac45594e5de85627085c2642d9b76dd7cfeea8 1508 pep-kde-settings_1.0-1peppermint1.debian.tar.xz
Checksums-Sha256:
 c7fde91e3aede527b11ec820d832276b40fae2817d89474f0d75a855ed6a395f 12192 pep-kde-settings_1.0.orig.tar.xz
 82173361b90fa50852e7c2a47a394f3772baff9ffa3c0b3fda2ada02ff2149e3 1508 pep-kde-settings_1.0-1peppermint1.debian.tar.xz
Files:
 42982a857f2601e85d1d0d0430515de7 12192 pep-kde-settings_1.0.orig.tar.xz
 8a9249477d5aad67b989ac7a2b263ce2 1508 pep-kde-settings_1.0-1peppermint1.debian.tar.xz
