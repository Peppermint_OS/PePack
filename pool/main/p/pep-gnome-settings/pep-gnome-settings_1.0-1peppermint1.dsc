Format: 3.0 (quilt)
Source: pep-gnome-settings
Binary: pep-gnome-settings
Architecture: all
Version: 1.0-1peppermint1
Maintainer: Manuel Rosa <manuelsilvarosa@gmail.com>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 pep-gnome-settings deb x11 optional arch=all
Checksums-Sha1:
 95606441719fb1f15781cd6db3637d175ff34900 10752 pep-gnome-settings_1.0.orig.tar.xz
 d769e761d3aa2f39738790e560f830ba8bb99e6a 1516 pep-gnome-settings_1.0-1peppermint1.debian.tar.xz
Checksums-Sha256:
 7cc5ce4b47ad670d9434519520f681486ec5f9ed74cff78e4ed02865bf1e6b52 10752 pep-gnome-settings_1.0.orig.tar.xz
 2aaa2e11fad18284cb89263a4ec75b67b0ac438e9575201292ece1ddc704b97b 1516 pep-gnome-settings_1.0-1peppermint1.debian.tar.xz
Files:
 1d8510034a30b323a4af7a5659498026 10752 pep-gnome-settings_1.0.orig.tar.xz
 20550594743917fc690646ecdf84af9b 1516 pep-gnome-settings_1.0-1peppermint1.debian.tar.xz
