Format: 3.0 (quilt)
Source: pepsources
Binary: pepsources
Architecture: all
Version: 1.0-1peppermint2
Maintainer: Manuel Rosa <manuelrosa@acoroslinux.tk>
Homepage: https://peppermintos.com/
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 pepsources deb system optional arch=all
Checksums-Sha1:
 4aba8cbe523199e2496bb310458b1da35a4366c9 512 pepsources_1.0.orig.tar.xz
 7081c8130b959fd31a0acf44771c7be291067e64 1280 pepsources_1.0-1peppermint2.debian.tar.xz
Checksums-Sha256:
 7a476da4161442b4fbcb4b6e644872184c5001f50af7c96e07febb192709e7a9 512 pepsources_1.0.orig.tar.xz
 0767a31fec650b45f88bcbcc08dce558e65f719c63225fee28d600b32c3f8703 1280 pepsources_1.0-1peppermint2.debian.tar.xz
Files:
 9c90992fb94fda7b01200984fb0ff777 512 pepsources_1.0.orig.tar.xz
 eda006da39c570f7b391f9ff3b7a83ab 1280 pepsources_1.0-1peppermint2.debian.tar.xz
