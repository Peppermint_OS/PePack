Format: 3.0 (quilt)
Source: pep-xfce-settings
Binary: pep-xfce-settings
Architecture: all
Version: 1.0-1peppermint1
Maintainer: Manuel Rosa <manuelsilvarosa@gmail.com>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 pep-xfce-settings deb x11 optional arch=all
Checksums-Sha1:
 544c720c95fce5a031f0dbd2e04d22b98498712f 13256 pep-xfce-settings_1.0.orig.tar.xz
 8bcbabafd6b287e21e28c1d60fab2681a345628c 1516 pep-xfce-settings_1.0-1peppermint1.debian.tar.xz
Checksums-Sha256:
 b4ad626a7770e1881cc43e85ebe81a21401d1e2c1d3e8093a7b8036e99e10425 13256 pep-xfce-settings_1.0.orig.tar.xz
 5dc22fbea52ed0626c5ff9b9c9f3af1c48b16218fdf9cb495cd8653c38d04319 1516 pep-xfce-settings_1.0-1peppermint1.debian.tar.xz
Files:
 4a5a26691bfa608da11e6358ae636968 13256 pep-xfce-settings_1.0.orig.tar.xz
 91af572b4900952fd9a3e49bb98f9f24 1516 pep-xfce-settings_1.0-1peppermint1.debian.tar.xz
