Format: 3.0 (quilt)
Source: pep-lxqt-settings
Binary: pep-lxqt-settings
Architecture: all
Version: 1.0-1peppermint1
Maintainer: Manuel Rosa <manuelsilvarosa@gmail.com>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 pep-lxqt-settings deb x11 optional arch=all
Checksums-Sha1:
 6315dd17f3d7349945ea8b42c4b59e28fe483304 15972 pep-lxqt-settings_1.0.orig.tar.xz
 90e0c1f68c2e3c6efc7636ff8ccc5e8ee9c3643a 1512 pep-lxqt-settings_1.0-1peppermint1.debian.tar.xz
Checksums-Sha256:
 8db2f06c8fe5c6e53f6acb40a908c369268b5dc13a59ee602348f86fe19cd2d5 15972 pep-lxqt-settings_1.0.orig.tar.xz
 9e725d273ff3efee9ba4562f68bef33c7335cf8b703cdbb59076a4aed02f1783 1512 pep-lxqt-settings_1.0-1peppermint1.debian.tar.xz
Files:
 0aa96a39bdeca862a91a9c9864e8376e 15972 pep-lxqt-settings_1.0.orig.tar.xz
 35acf740d54b062f2c1459d5f57314be 1512 pep-lxqt-settings_1.0-1peppermint1.debian.tar.xz
