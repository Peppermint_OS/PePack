Format: 3.0 (quilt)
Source: pep-desktop-settings
Binary: pep-desktop-settings
Architecture: any
Version: 1.0-1peppermint2
Maintainer: Manuel Rosa <manuelsilvarosa@gmail.com>
Homepage: <insert the upstream URL, if relevant>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 pep-desktop-settings deb unknown optional arch=any
Checksums-Sha1:
 99fb479cdeabe4d6a2f942f2c1f87939d60a0c97 13976 pep-desktop-settings_1.0.orig.tar.xz
 6fa0b11798091c1f64dcc65343b924222d8ae6e5 8048 pep-desktop-settings_1.0-1peppermint2.debian.tar.xz
Checksums-Sha256:
 524aad4677ab38842326315ad4457e0bae5e2f8c0e0bc9bee470f89f8e806d12 13976 pep-desktop-settings_1.0.orig.tar.xz
 d534f7b31cba355e0034b64016bbb21d0d861876aa7cdc1a0a3b1ae1eee136a8 8048 pep-desktop-settings_1.0-1peppermint2.debian.tar.xz
Files:
 059a3ad8ea5816b752e73937db096e3b 13976 pep-desktop-settings_1.0.orig.tar.xz
 6b32bde64b50cc088f33b383956d46eb 8048 pep-desktop-settings_1.0-1peppermint2.debian.tar.xz
