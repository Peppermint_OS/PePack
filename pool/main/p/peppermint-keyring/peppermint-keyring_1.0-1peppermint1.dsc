Format: 3.0 (quilt)
Source: peppermint-keyring
Binary: peppermint-keyring
Architecture: all
Version: 1.0-1peppermint1
Maintainer: Manuel Rosa <manuelrosa@acoroslinux.pt>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 peppermint-keyring deb system optional arch=all
Checksums-Sha1:
 c14af659c662a41cb2fba2f0eedcca2a0357b234 2480 peppermint-keyring_1.0.orig.tar.xz
 1b074e1569be271c076467cbbbb1fb654e4ac7f0 1524 peppermint-keyring_1.0-1peppermint1.debian.tar.xz
Checksums-Sha256:
 33b9459f16f79822878aa039fcc17c26254534c7d51f29b82800d1d97039cddb 2480 peppermint-keyring_1.0.orig.tar.xz
 5d9e4e734ecb85e3c2fe57be73aee46a68060532f96140ccec54f92179fb2ed8 1524 peppermint-keyring_1.0-1peppermint1.debian.tar.xz
Files:
 9b81458c64d88199258b2aab467bc600 2480 peppermint-keyring_1.0.orig.tar.xz
 e57df777771bb276f1bc39cf5561611b 1524 peppermint-keyring_1.0-1peppermint1.debian.tar.xz
