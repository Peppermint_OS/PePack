Format: 3.0 (quilt)
Source: peppermint-icon-themes
Binary: peppermint-icon-themes
Architecture: all
Version: 03092022-1
Maintainer: Manuel Rosa <manuelrosa@acoroslinux.tk>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 peppermint-icon-themes deb x11 optional arch=all
Checksums-Sha1:
 69a143d66210461ad9c7bdfb53757430506bff8b 6722108 peppermint-icon-themes_03092022.orig.tar.xz
 175fc52b6da725d1adb168457eea4a91262ca948 1192 peppermint-icon-themes_03092022-1.debian.tar.xz
Checksums-Sha256:
 480948acb579a1d482a0777f63c85f6613c7c7bca1cd43d1fe914c7afbb09a50 6722108 peppermint-icon-themes_03092022.orig.tar.xz
 4a2ca48dae4245a28051599ebcf3e457cc8d348f147fd24cfeb2510f14a70e92 1192 peppermint-icon-themes_03092022-1.debian.tar.xz
Files:
 5eb93a17894ccb65a347673d74cbb476 6722108 peppermint-icon-themes_03092022.orig.tar.xz
 ef8f87e42d5854954064a11097fd8a1e 1192 peppermint-icon-themes_03092022-1.debian.tar.xz
