Format: 3.0 (quilt)
Source: peppermint-wallpapers
Binary: peppermint-wallpapers
Architecture: all
Version: 04052023-2peppermint5
Maintainer: Manuel Rosa <manuelrosa@acoroslinux.tk>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 peppermint-wallpapers deb x11 optional arch=all
Checksums-Sha1:
 dabfe34107540addb2ca3e5de8cb4c28daebbe0c 3010888 peppermint-wallpapers_04052023.orig.tar.xz
 f612f219343d9ce86bdee2be5c10b76488c670bd 1500 peppermint-wallpapers_04052023-2peppermint5.debian.tar.xz
Checksums-Sha256:
 f3a5c63ac6001771f8d4c2cf3a5ae90b6cce8f9eab4d23b541be3f829a960b5b 3010888 peppermint-wallpapers_04052023.orig.tar.xz
 8b28021a3f1c22f548e35c066b134a22d47753213c96f822d5d3385fb6b2ef16 1500 peppermint-wallpapers_04052023-2peppermint5.debian.tar.xz
Files:
 e326351131de0abce6dba18499dfa187 3010888 peppermint-wallpapers_04052023.orig.tar.xz
 dff71499fea0b1d787039ffaba7b6c1c 1500 peppermint-wallpapers_04052023-2peppermint5.debian.tar.xz
