Format: 3.0 (quilt)
Source: pep-mate-settings
Binary: pep-mate-settings
Architecture: all
Version: 1.0-1peppermint1
Maintainer: Manuel Rosa <manuelsilvarosa@gmail.com>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 pep-mate-settings deb x11 optional arch=all
Checksums-Sha1:
 796886599e92a187e9dda54f4c7449f103fc32fe 13452 pep-mate-settings_1.0.orig.tar.xz
 40f1fa0ae4f8e738a765bf72bc74d9889f0d8ab0 1516 pep-mate-settings_1.0-1peppermint1.debian.tar.xz
Checksums-Sha256:
 96511ecafb3fd5396a10d5cc2c555458c02da52a616e17d22e9c40ed71f756bc 13452 pep-mate-settings_1.0.orig.tar.xz
 83d99f973e001f9c1e18cbb27b0240d806dfc2bd26d1188878ce99a659980ffe 1516 pep-mate-settings_1.0-1peppermint1.debian.tar.xz
Files:
 7293258a424e81bfb2830bbd2cd17313 13452 pep-mate-settings_1.0.orig.tar.xz
 0f2d75330acf10373b5f8e1f41f24e42 1516 pep-mate-settings_1.0-1peppermint1.debian.tar.xz
