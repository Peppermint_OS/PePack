Format: 3.0 (quilt)
Source: pep-budgie-settings
Binary: pep-budgie-settings
Architecture: all
Version: 1.0-1peppermint1
Maintainer: Manuel Rosa <manuelsilvarosa@gmail.com>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 pep-budgie-settings deb x11 optional arch=all
Checksums-Sha1:
 5ea328fd449c4b6d9047134cc58226e005221f29 11016 pep-budgie-settings_1.0.orig.tar.xz
 c302d8e84f6d1c3b28f765013a9c9897ad6caa68 1692 pep-budgie-settings_1.0-1peppermint1.debian.tar.xz
Checksums-Sha256:
 e2e1f235395f4c8cd71aaae7dd0416b1e1bb4709daa1ece8b512d3bf299c47ad 11016 pep-budgie-settings_1.0.orig.tar.xz
 a0b6272dac9fd832f6d20800639ac53bdf487c372dc65928e0e80d2844373ea9 1692 pep-budgie-settings_1.0-1peppermint1.debian.tar.xz
Files:
 90bcbdf0503c0e728794c92fe00ff219 11016 pep-budgie-settings_1.0.orig.tar.xz
 fe031ef05694d30acef88bbb1c6fc20f 1692 pep-budgie-settings_1.0-1peppermint1.debian.tar.xz
