Format: 3.0 (quilt)
Source: pep-cinnamon-settings
Binary: pep-cinnamon-settings
Architecture: all
Version: 1.0-1peppermint5
Maintainer: Manuel Rosa <manuelsilvarosa@gmail.com>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 pep-cinnamon-settings deb x11 optional arch=all
Checksums-Sha1:
 6c1286fcb965c7319576a8c13fb4fb7398b6d1dd 13884 pep-cinnamon-settings_1.0.orig.tar.xz
 927d31d13092d024e67b069b3c1c7592c5e70779 1668 pep-cinnamon-settings_1.0-1peppermint5.debian.tar.xz
Checksums-Sha256:
 7533fc783c2aaa6f5990445ca52940d3820fa86c8e2892871f9eed745cd5cbe3 13884 pep-cinnamon-settings_1.0.orig.tar.xz
 242fc4a08bfa789550dccaaab8b59ad80dce3652859a7dd01b4adcacda6d5625 1668 pep-cinnamon-settings_1.0-1peppermint5.debian.tar.xz
Files:
 2f0feb065779a1969fd8d824a330985f 13884 pep-cinnamon-settings_1.0.orig.tar.xz
 904cd1069917643f1a21087a442e4db0 1668 pep-cinnamon-settings_1.0-1peppermint5.debian.tar.xz
