Format: 3.0 (quilt)
Source: tela-icon-theme
Binary: tela-icon-theme
Architecture: all
Version: 1.0.1-1peppermint1
Maintainer: Manuel Rosa <manuelsilvarosa@gmail.com>
Homepage: https://github.com/vinceliuice/Tela-icon-theme
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 tela-icon-theme deb misc optional arch=all
Checksums-Sha1:
 6d6bad269b651242e0f5b4437d272ae1d23d7614 10910352 tela-icon-theme_1.0.1.orig.tar.xz
 ec72b0842d738404ad1e6475e7f2901df025f175 1896 tela-icon-theme_1.0.1-1peppermint1.debian.tar.xz
Checksums-Sha256:
 24bbe5a9810aec5971faea31b950b57ecd70be1859a3aacce0c38d3ec6f93779 10910352 tela-icon-theme_1.0.1.orig.tar.xz
 2d10cea6aef46508b71c287f624921640cda03e9ce362f06b4dffd82fc0674c7 1896 tela-icon-theme_1.0.1-1peppermint1.debian.tar.xz
Files:
 1e1e614a8cabdb9ccdfe745b65ef16f3 10910352 tela-icon-theme_1.0.1.orig.tar.xz
 5ac924e103d066652f12692311601601 1896 tela-icon-theme_1.0.1-1peppermint1.debian.tar.xz
