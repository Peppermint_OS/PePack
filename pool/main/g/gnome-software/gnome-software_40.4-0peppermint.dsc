Format: 3.0 (quilt)
Source: gnome-software
Binary: gnome-software, gnome-software-common, gnome-software-plugin-flatpak, gnome-software-plugin-snap, gnome-software-dev, gnome-software-doc
Architecture: any all
Version: 40.4-0peppermint
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Laurent Bigonville <bigon@debian.org>, Matthias Klumpp <mak@debian.org>
Homepage: https://wiki.gnome.org/Apps/Software
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/gnome-team/gnome-software
Vcs-Git: https://salsa.debian.org/gnome-team/gnome-software.git
Build-Depends: appstream, appstream-util, debhelper-compat (= 13), docbook-xsl, gnome-pkg-tools (>= 0.10), gsettings-desktop-schemas-dev (>= 3.18), gtk-doc-tools, libappstream-glib-dev (>= 0.7.14), libflatpak-dev (>= 1.0.4) [amd64 arm64 armel armhf i386 mips mipsel mips64el ppc64el s390x hppa powerpc powerpcspe ppc64], libfwupd-dev (>= 1.0.3) [linux-any], libglib2.0-dev (>= 2.56.0), libgnome-desktop-3-dev (>= 3.18.0), libgoa-1.0-dev, libgspell-1-dev, libgtk-3-dev (>= 3.22.4), libgudev-1.0-dev [linux-any], libjson-glib-dev (>= 1.2.0), libostree-dev [linux-any], libmalcontent-0-dev [amd64 arm64 armel armhf i386 mips mipsel mips64el ppc64el s390x hppa powerpc powerpcspe ppc64], libpackagekit-glib2-dev (>= 1.1.11), libpolkit-gobject-1-dev, libsnapd-glib-dev (>= 1.54) [amd64 arm64 armel armhf i386 ppc64el s390x], libsoup2.4-dev (>= 2.52.0), libxml2-utils, libxmlb-dev (>= 0.1.7), meson (>= 0.47), pkg-config, policykit-1, valgrind [amd64 arm64 armhf i386 mips mips64 mips64el mipsel powerpc ppc64 ppc64el s390x], xsltproc, libappstream-dev, libhandy-1-dev, libjcat-dev, libcurl4-gnutls-dev
Build-Depends-Indep: appstream-glib-doc <!nodoc>, libgdk-pixbuf2.0-doc <!nodoc>, libglib2.0-doc <!nodoc>, libgtk-3-doc <!nodoc>, libjson-glib-doc <!nodoc>, libsoup2.4-doc <!nodoc>
Package-List:
 gnome-software deb gnome optional arch=any
 gnome-software-common deb gnome optional arch=all
 gnome-software-dev deb libdevel optional arch=any
 gnome-software-doc deb doc optional arch=all profile=!nodoc
 gnome-software-plugin-flatpak deb gnome optional arch=amd64,arm64,armel,armhf,hppa,i386,mips,mips64el,powerpc,powerpcspe,ppc64,ppc64el,s390x
 gnome-software-plugin-snap deb gnome optional arch=amd64,arm64,armel,armhf,i386,ppc64el,s390x
Checksums-Sha1:
 7c9b9c4a7c24fe1634c49b63ad4f478aa16e10c4 3098652 gnome-software_40.4.orig.tar.xz
 d488966c08f7a372a5859d709ae484227dd8fddc 22100 gnome-software_40.4-0peppermint.debian.tar.xz
Checksums-Sha256:
 c3a4a1b3956da1d0ec7abaeb723982d4ef0a10e9c37d40d64e57fe2780fc3767 3098652 gnome-software_40.4.orig.tar.xz
 d3e1efcc9f445b81686e831db199c72894c42d6997cab9a82618707acfe19785 22100 gnome-software_40.4-0peppermint.debian.tar.xz
Files:
 cd35c96ba149430dd4a083f7bea90e90 3098652 gnome-software_40.4.orig.tar.xz
 58eae11872ef9d25d762d0b8abf39813 22100 gnome-software_40.4-0peppermint.debian.tar.xz
