Format: 3.0 (quilt)
Source: xfce4-time-out-plugin
Binary: xfce4-time-out-plugin
Architecture: any
Version: 1.1.2-2~peppermint3
Maintainer: SwampRabbit <swamprabbit@mxlinux.org>
Homepage: https://docs.xfce.org/panel-plugins/xfce4-time-out-plugin
Standards-Version: 4.5.0
Build-Depends: debhelper-compat (= 13), intltool, libxfce4panel-2.0-dev, libxfce4ui-2-dev, xfce4-dev-tools
Package-List:
 xfce4-time-out-plugin deb x11 optional arch=any
Checksums-Sha1:
 b0dddac365cddfb07f9f79f9700ee961150c9dc8 332848 xfce4-time-out-plugin_1.1.2.orig.tar.xz
 9382ad3d5a51ddb895691355c09a0c5e3046d6c9 3256 xfce4-time-out-plugin_1.1.2-2~peppermint3.debian.tar.xz
Checksums-Sha256:
 264b3bb3a8ba5424e954ff0ea0f80e8225d0497d3924aebc742925e302ad9838 332848 xfce4-time-out-plugin_1.1.2.orig.tar.xz
 e9ec5f8530873802f1ad480873d30d61e87b0d68d8b95d2b392e8e1878d94dee 3256 xfce4-time-out-plugin_1.1.2-2~peppermint3.debian.tar.xz
Files:
 5c8d8258bd9b7f8e4d49cc8131f03ed5 332848 xfce4-time-out-plugin_1.1.2.orig.tar.xz
 e9654c75a17a89dc3a81fe89af8d04e3 3256 xfce4-time-out-plugin_1.1.2-2~peppermint3.debian.tar.xz
Original-Maintainer: Xubuntu Developers <xubuntu-devel@lists.ubuntu.com>
