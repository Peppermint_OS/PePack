Format: 3.0 (quilt)
Source: xfce4-panel-profiles
Binary: xfce4-panel-profiles, xfpanel-switch
Architecture: all
Version: 1.0.10-0peppermint1
Maintainer: Timothy E. Harris <maintainer@mxrepo.com>
Homepage: https://git.xfce.org/apps/xfce4-panel-profiles/about/
Standards-Version: 4.3.0
Build-Depends: debhelper (>= 11), dh-python, python3, python3-distutils-extra (>= 2.18)
Package-List:
 xfce4-panel-profiles deb xfce optional arch=all
 xfpanel-switch deb oldlibs optional arch=all
Checksums-Sha1:
 838a069c9cef082fe59c66e3a5fc3668d96d3806 60112 xfce4-panel-profiles_1.0.10.orig.tar.xz
 10439f5d2b24feadd85b3571021f698ed984378d 2544 xfce4-panel-profiles_1.0.10-0peppermint1.debian.tar.xz
Checksums-Sha256:
 90bc2837dd5ca73353e196371944a7ee8cd45e83f7766f600797cdde0126fbd2 60112 xfce4-panel-profiles_1.0.10.orig.tar.xz
 c6de46f747cad0d96d9ed771b0ab724369e24e876782dfebeeeff65e32f10019 2544 xfce4-panel-profiles_1.0.10-0peppermint1.debian.tar.xz
Files:
 e1ebf8dff0137dc971b32a1dc5520aa0 60112 xfce4-panel-profiles_1.0.10.orig.tar.xz
 d6439cb309963fbef2513da8127228c1 2544 xfce4-panel-profiles_1.0.10-0peppermint1.debian.tar.xz
Original-Maintainer: Xubuntu Developers <xubuntu-devel@lists.ubuntu.com>
