Format: 3.0 (quilt)
Source: xfce4-notes-plugin
Binary: xfce4-notes, xfce4-notes-plugin
Architecture: any
Version: 1.9.0-1~peppermint2
Maintainer: SwampRabbit <swamprabbit@mxlinux.org>
Uploaders: Yves-Alexis Perez <corsac@debian.org>
Homepage: https://goodies.xfce.org/projects/panel-plugins/xfce4-notes-plugin
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/xfce-team/goodies/xfce4-notes-plugin
Vcs-Git: https://salsa.debian.org/xfce-team/goodies/xfce4-notes-plugin.git
Build-Depends: automake, debhelper-compat (= 12), intltool, libglib2.0-dev, libgtk-3-dev, libtool, libx11-dev, libxfce4panel-2.0-dev (>= 4.14), libxfce4ui-2-dev (>= 4.14), libxfce4util-dev (>= 4.14), libxfconf-0-dev (>= 4.14), pkg-config, xfce4-dev-tools (>= 4.16)
Package-List:
 xfce4-notes deb xfce optional arch=any
 xfce4-notes-plugin deb xfce optional arch=any
Checksums-Sha1:
 4c204b7379a3b39457398377fa28c07ff019648d 414824 xfce4-notes-plugin_1.9.0.orig.tar.xz
 1ccbeee867339eb03f8a5bc4640ba1002fb1e8cd 5392 xfce4-notes-plugin_1.9.0-1~peppermint2.debian.tar.xz
Checksums-Sha256:
 8979af98f1771f41e0cb60912ce96c0c66654aaf43a023210f384d97d0177d93 414824 xfce4-notes-plugin_1.9.0.orig.tar.xz
 ba8117cca4066dc9a75cd475a03f922a27bad2643e7cae4b9cc795009bd92210 5392 xfce4-notes-plugin_1.9.0-1~peppermint2.debian.tar.xz
Files:
 fe743192e6c69c5ab21992e9688e8b0d 414824 xfce4-notes-plugin_1.9.0.orig.tar.xz
 6e9bab98bb719e09396010b41fe45ecf 5392 xfce4-notes-plugin_1.9.0-1~peppermint2.debian.tar.xz
Original-Maintainer: Debian Xfce Maintainers <debian-xfce@lists.debian.org>
