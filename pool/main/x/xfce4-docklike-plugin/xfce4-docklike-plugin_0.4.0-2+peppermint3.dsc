Format: 3.0 (quilt)
Source: xfce4-docklike-plugin
Binary: xfce4-docklike-plugin
Architecture: any
Version: 0.4.0-2+peppermint3
Maintainer: SwampRabbit <swamprabbit@mxlinux.org>
Homepage: https://github.com/davekeogh/xfce4-docklike-plugin
Standards-Version: 4.5.0
Build-Depends: debhelper-compat (= 13), intltool, libtool, libx11-dev, xfce4-dev-tools, libxfce4ui-2-dev, libxfce4panel-2.0-dev, libxfce4util-dev, libgtk-3-dev, libcairo2-dev, libwnck-3-dev, libglib2.0-dev, libexo-2-dev
Package-List:
 xfce4-docklike-plugin deb xfce optional arch=any
Checksums-Sha1:
 4ad94612a9d6a437a3963dc945b39074e452cc9f 308312 xfce4-docklike-plugin_0.4.0.orig.tar.xz
 01a219f21c02208216fe5933b1ae4ce64cf2cb6a 2848 xfce4-docklike-plugin_0.4.0-2+peppermint3.debian.tar.xz
Checksums-Sha256:
 a021cad1bec2a1cd24e0ef3a3e877b3c3790944bf16cbe33d7255921abcfccc7 308312 xfce4-docklike-plugin_0.4.0.orig.tar.xz
 927e17cbd4b8a6d4355b2d1cdac0eabd7ebedc57a80643eb21a0c96a610810ab 2848 xfce4-docklike-plugin_0.4.0-2+peppermint3.debian.tar.xz
Files:
 f45179326c2659bdb306af666f0f880d 308312 xfce4-docklike-plugin_0.4.0.orig.tar.xz
 b06371b3a99bd654e3b0d160bd868931 2848 xfce4-docklike-plugin_0.4.0-2+peppermint3.debian.tar.xz
Original-Maintainer: Nicolas Szabo <nszabo@vivaldi.net>, David Keogh <davidtkeogh@gmail.com>
