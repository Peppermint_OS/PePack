Format: 3.0 (native)
Source: xfce4-peppermintmenu-plugin
Binary: xfce4-peppermintmenu-plugin
Architecture: any
Version: 1.1.4peppermint1
Maintainer: ManuelRosa <manuelsilvarosa@gmail.com>
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 9), cmake, pkg-config, gettext, libexo-2-dev, libgarcon-1-0-dev, libgtk-3-dev, libxfce4ui-2-dev, libxfce4util-dev, libxfce4panel-2.0-dev
Package-List:
 xfce4-peppermintmenu-plugin deb xfce optional arch=any
Checksums-Sha1:
 849a51c50ea6672cfb7609225096f63959e77b28 290392 xfce4-peppermintmenu-plugin_1.1.4peppermint1.tar.xz
Checksums-Sha256:
 c858b3ae1337e0f3c63d291fa550c634f7d24bfc6196162ccba651b659b0bb71 290392 xfce4-peppermintmenu-plugin_1.1.4peppermint1.tar.xz
Files:
 8534526b25cad74bc09e95c1fc610648 290392 xfce4-peppermintmenu-plugin_1.1.4peppermint1.tar.xz
