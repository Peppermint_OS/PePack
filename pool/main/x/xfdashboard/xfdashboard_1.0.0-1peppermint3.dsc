Format: 3.0 (quilt)
Source: xfdashboard
Binary: xfdashboard, xfdashboard-plugins, libxfdashboard0, libxfdashboard-dev
Architecture: any
Version: 1.0.0-1peppermint3
Maintainer: Timothy E. Harris <maintainer@mxrepo.com>
Uploaders: Unit 193 <unit193@ubuntu.com>
Homepage: https://docs.xfce.org/apps/xfdashboard/start
Standards-Version: 4.5.1
Vcs-Browser: https://github.com/Xubuntu/xfdashboard
Vcs-Git: https://github.com/Xubuntu/xfdashboard.git
Build-Depends: debhelper-compat (= 12), gettext, intltool, libclutter-1.0-dev, libdbus-glib-1-dev, libgarcon-1-0-dev, libglib2.0-dev, libwnck-3-dev, libxcomposite-dev, libxdamage-dev, libxfce4ui-2-dev, libxfce4util-dev, libxfconf-0-dev, pkg-config, xfce4-dev-tools
Package-List:
 libxfdashboard-dev deb libdevel optional arch=any
 libxfdashboard0 deb libs optional arch=any
 xfdashboard deb xfce optional arch=any
 xfdashboard-plugins deb xfce optional arch=any
Checksums-Sha1:
 d243066b3eb8b82167d7ea9e65fbb36bb658a43a 3039416 xfdashboard_1.0.0.orig.tar.xz
 576be15e14c270bbf27f115c46a8f719a6c45621 3884 xfdashboard_1.0.0-1peppermint3.debian.tar.xz
Checksums-Sha256:
 cc676e25b3d3dd2b5923d98b111a65c0876dedce602d533696d6fd8389c9ff77 3039416 xfdashboard_1.0.0.orig.tar.xz
 c7b4d02bd18ad0ec3d8bbb949536c9c28c906a960a855a665dae570d21c17e86 3884 xfdashboard_1.0.0-1peppermint3.debian.tar.xz
Files:
 e684962b7f89ebfc8b499dd0b17b53f9 3039416 xfdashboard_1.0.0.orig.tar.xz
 3f0d8bec665e2d2df195e721da0c21ed 3884 xfdashboard_1.0.0-1peppermint3.debian.tar.xz
Original-Maintainer: Xubuntu Developers <xubuntu-devel@lists.ubuntu.com>
