Format: 3.0 (quilt)
Source: libva
Binary: libva-dev, libva2, libva-x11-2, libva-glx2, libva-drm2, libva-wayland2, va-driver-all
Architecture: any
Version: 2.13.0-1~peppermint1
Maintainer: SwampRabbit <swamprabbit@mxlinux.org>
Uploaders:  Reinhard Tartler <siretart@tauware.de>, Sebastian Ramacher <sramacher@debian.org>
Homepage: https://01.org/linuxmedia/vaapi
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/multimedia-team/libva
Vcs-Git: https://salsa.debian.org/multimedia-team/libva.git
Build-Depends: debhelper-compat (= 13), libdrm-dev, libgl1-mesa-dev | libgl-dev, libwayland-dev (>= 1.0.0) [linux-any], libx11-dev, libxext-dev, libxfixes-dev, pkg-config, perl:any
Package-List:
 libva-dev deb libdevel optional arch=any
 libva-drm2 deb libs optional arch=any
 libva-glx2 deb libs optional arch=any
 libva-wayland2 deb libs optional arch=linux-any
 libva-x11-2 deb libs optional arch=any
 libva2 deb libs optional arch=any
 va-driver-all deb video optional arch=any
Checksums-Sha1:
 519444a1c96d8f485d8e56d4445eb53168c9c4e1 256724 libva_2.13.0.orig.tar.gz
 cbf9e3cc191d268c9923a03917930a03f4328c2b 11908 libva_2.13.0-1~peppermint1.debian.tar.xz
Checksums-Sha256:
 6b7ec7d4fa204dad3f266450981f1f0892400c03afd3e00ac11f8ccade5aaaa1 256724 libva_2.13.0.orig.tar.gz
 db5430068c939bbf0a352d53c0cbfd314e0f2372a61463e204dcadd30f7e76a0 11908 libva_2.13.0-1~peppermint1.debian.tar.xz
Files:
 d8753a1ed468b5a8ac343d0ec64d0f13 256724 libva_2.13.0.orig.tar.gz
 da069659c0266b4deb97a92e3dcba81c 11908 libva_2.13.0-1~peppermint1.debian.tar.xz
Original-Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
