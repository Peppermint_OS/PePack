Format: 3.0 (quilt)
Source: citrus-red-icon-theme
Binary: citrus-red-icon-theme
Architecture: all
Version: 1.0.0-1
Maintainer: Manuel Rosa <manuelsilvarosa@gmail.com>
Homepage: https://github.com/yeyushengfan258/Citrus-icon-theme
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 citrus-red-icon-theme deb misc optional arch=all
Checksums-Sha1:
 e60e6e78a5238ec01340399ddfeb1f7af122e121 2926508 citrus-red-icon-theme_1.0.0.orig.tar.xz
 2b6cf52bcdcddc340bb6f113729fa980bc427654 2076 citrus-red-icon-theme_1.0.0-1.debian.tar.xz
Checksums-Sha256:
 ccaff93b98e1ae096b5a1f348a7d9943e8d5432dc7f1531cf3ccc63e3e24fc2a 2926508 citrus-red-icon-theme_1.0.0.orig.tar.xz
 1fb716d726354fc9f205cc328d301df51e699b353e7f67dc1bb83f73d6ec8b16 2076 citrus-red-icon-theme_1.0.0-1.debian.tar.xz
Files:
 2428b85036a4e0fc7117e02a6fb04560 2926508 citrus-red-icon-theme_1.0.0.orig.tar.xz
 52f5ce118abab11ce1d46cc8baf6878c 2076 citrus-red-icon-theme_1.0.0-1.debian.tar.xz
