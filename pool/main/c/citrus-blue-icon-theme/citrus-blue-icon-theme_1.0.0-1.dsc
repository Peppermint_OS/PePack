Format: 3.0 (quilt)
Source: citrus-blue-icon-theme
Binary: citrus-blue-icon-theme
Architecture: all
Version: 1.0.0-1
Maintainer: Manuel Rosa <manuelsilvarosa@gmail.com>
Homepage: https://github.com/yeyushengfan258/Citrus-icon-theme
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 citrus-blue-icon-theme deb misc optional arch=all
Checksums-Sha1:
 42dda7635d74deba5ae59314be320e1fa4820908 2926644 citrus-blue-icon-theme_1.0.0.orig.tar.xz
 61dd39ecc507811dd27662b5e20c3f5dbde53562 2076 citrus-blue-icon-theme_1.0.0-1.debian.tar.xz
Checksums-Sha256:
 99d7763c55ea28a00ff5a75c7c499770a428ee2ce02c46c2a075645975ec7b2d 2926644 citrus-blue-icon-theme_1.0.0.orig.tar.xz
 653a12bec1035c7c45c6999ce74e636ce651ea840d5bac0ffa14bf3e3121c8a3 2076 citrus-blue-icon-theme_1.0.0-1.debian.tar.xz
Files:
 f0f458b1c994acfed8a68f9e658c48a6 2926644 citrus-blue-icon-theme_1.0.0.orig.tar.xz
 c9531dfd11034755e3b704270c4e1100 2076 citrus-blue-icon-theme_1.0.0-1.debian.tar.xz
