Format: 3.0 (native)
Source: cinnamon-desktop-environment
Binary: cinnamon-core, cinnamon-desktop-environment
Architecture: all
Version: 1:5.6.0peppermint1
Maintainer: Debian Cinnamon Team <debian-cinnamon@lists.debian.org>
Uploaders:  Maximiliano Curia <maxy@debian.org>, Margarita Manterola <marga@debian.org>, Fabio Fantoni <fantonifabio@tiscali.it>, Norbert Preining <norbert@preining.info>, Christoph Martin <martin@uni-mainz.de>
Homepage: http://cinnamon.linuxmint.com/
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/cinnamon-team/cinnamon-desktop-environment
Vcs-Git: https://salsa.debian.org/cinnamon-team/cinnamon-desktop-environment.git
Build-Depends: debhelper-compat (= 13)
Package-List:
 cinnamon-core deb metapackages optional arch=all
 cinnamon-desktop-environment deb metapackages optional arch=all
Checksums-Sha1:
 4c37cb2d89df50cca84939cc97b1a0770dc607b1 4668 cinnamon-desktop-environment_5.6.0peppermint1.tar.xz
Checksums-Sha256:
 ce09433f488b7d0da192fe6d119c869a2f51e023d5a3f1e4eaf30cd1909d46e5 4668 cinnamon-desktop-environment_5.6.0peppermint1.tar.xz
Files:
 d9b0f53ae1b60efd9cd472387ec636bf 4668 cinnamon-desktop-environment_5.6.0peppermint1.tar.xz
