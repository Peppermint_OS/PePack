Format: 3.0 (quilt)
Source: calamares
Binary: calamares
Architecture: linux-any
Version: 3.2.57-2peppermint2
Maintainer: Jonathan Carter <jcc@debian.org>
Homepage: https://github.com/calamares/calamares
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/calamares
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/calamares.git
Build-Depends: cmake, cryptsetup <!nocheck>, debhelper-compat (= 13), extra-cmake-modules, gettext, libboost-python-dev, libkf5config-dev, libkf5coreaddons-dev, libkf5i18n-dev, libkf5iconthemes-dev, libkf5kio-dev, libkf5parts-dev, libkf5plasma-dev, libkf5service-dev, libkf5solid-dev, libkpmcore-dev, libparted-dev, libpolkit-qt5-1-dev, libpwquality-dev, libqt5svg5-dev, libqt5webkit5-dev, libyaml-cpp-dev, os-prober <!nocheck>, pkg-config, pkg-kde-tools, policykit-1 <!nocheck>, python3-dev, qml-module-qtquick-layouts, qml-module-qtquick-privatewidgets, qml-module-qtquick-window2, qml-module-qtquick2, qtbase5-dev, qtdeclarative5-dev, qttools5-dev, qttools5-dev-tools
Package-List:
 calamares deb utils optional arch=linux-any
Checksums-Sha1:
 a56903667ef289d48b9c2ff79c55276ece650424 2403680 calamares_3.2.57.orig.tar.xz
 d45f2adf88bcc60d1e66d105af3250142ece279b 8424 calamares_3.2.57-2peppermint2.debian.tar.xz
Checksums-Sha256:
 383ee3dd9630c992cee0d421213836205903ae33e964a9e6928fd8e4d14d2ac8 2403680 calamares_3.2.57.orig.tar.xz
 74f998b7e32687fb5121e5e590e672c9ba40adeefe8ffc01c879e706a1421dd9 8424 calamares_3.2.57-2peppermint2.debian.tar.xz
Files:
 c2776758b4888f818f6968ac3fac98be 2403680 calamares_3.2.57.orig.tar.xz
 a2e89a2e72ef4e08a53d6f7a43b604de 8424 calamares_3.2.57-2peppermint2.debian.tar.xz
