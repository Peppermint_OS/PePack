Format: 3.0 (quilt)
Source: calamares
Binary: calamares
Architecture: linux-any
Version: 3.2.61-3peppermint5
Maintainer: Jonathan Carter <jcc@debian.org>
Homepage: https://github.com/calamares/calamares
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/calamares
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/calamares.git
Build-Depends: cmake, cryptsetup <!nocheck>, debhelper-compat (= 13), extra-cmake-modules, gettext, libboost-python-dev, libkf5config-dev, libkf5coreaddons-dev, libkf5i18n-dev, libkf5iconthemes-dev, libkf5kio-dev, libkf5parts-dev, libkf5plasma-dev, libkf5service-dev, libkf5solid-dev, libkpmcore-dev, libparted-dev, libpolkit-qt5-1-dev, libpwquality-dev, libqt5svg5-dev, libqt5webkit5-dev, libyaml-cpp-dev, os-prober <!nocheck>, pkg-config, pkg-kde-tools, policykit-1 <!nocheck>, python3-dev, qml-module-qtquick-layouts, qml-module-qtquick-privatewidgets, qml-module-qtquick-window2, qml-module-qtquick2, qtbase5-dev, qtdeclarative5-dev, qttools5-dev, qttools5-dev-tools
Package-List:
 calamares deb utils optional arch=linux-any
Checksums-Sha1:
 317aadcc2dd74448a5bb772f56159bc000f26acc 2406684 calamares_3.2.61.orig.tar.xz
 9ec0b1e368697e3cff5ebf88e677290402675caa 9284 calamares_3.2.61-3peppermint5.debian.tar.xz
Checksums-Sha256:
 9b29608dd794cbd1dabdb2d59cee30302ed90ed00701463e45f4b697c92e31a8 2406684 calamares_3.2.61.orig.tar.xz
 9b972bb8bb6c8be96cd4bf7ae2a1cf2ab3b901455e172ca48553b8e627636d72 9284 calamares_3.2.61-3peppermint5.debian.tar.xz
Files:
 4acfa58349d38c5682f35144dab32333 2406684 calamares_3.2.61.orig.tar.xz
 d21339fd49e9b3949e8001e23d24a3df 9284 calamares_3.2.61-3peppermint5.debian.tar.xz
