Format: 3.0 (quilt)
Source: citrus-icon-theme
Binary: citrus-icon-theme
Architecture: all
Version: 1.0.0-1
Maintainer: Manuel Rosa <manuelsilvarosa@gmail.com>
Homepage: https://github.com/yeyushengfan258/Citrus-icon-theme
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 citrus-icon-theme deb misc optional arch=all
Checksums-Sha1:
 0d5e2b18fd74500d24d9bd66563b13289e246611 2929184 citrus-icon-theme_1.0.0.orig.tar.xz
 d74262366557a80e031176a9858ee6619a8f313b 2068 citrus-icon-theme_1.0.0-1.debian.tar.xz
Checksums-Sha256:
 d1190e6ddbffda7c472fbc19c90d648e4efdb75c3f10e7b2f005283233839e06 2929184 citrus-icon-theme_1.0.0.orig.tar.xz
 3fa4a1420071e1616c5b93e71c5e9bb1bd87da25fb0bb1c51e6f6ca8a2324f44 2068 citrus-icon-theme_1.0.0-1.debian.tar.xz
Files:
 b5af1d7117a5b63f1873e3f7ce6301a0 2929184 citrus-icon-theme_1.0.0.orig.tar.xz
 6918070495057a9927f51515c4bd67fd 2068 citrus-icon-theme_1.0.0-1.debian.tar.xz
