Format: 3.0 (quilt)
Source: rtl8812au
Binary: rtl8812au-dkms
Architecture: all
Version: 5.13.6+git20220728-0~peppermint1
Maintainer: Timothy E. Harris <maintainer@mxrepo.com>
Homepage: https://github.com/morrownr/8812au-20210629
Standards-Version: 4.1.3
Build-Depends: debhelper-compat (= 13), dkms, dh-exec
Package-List:
 rtl8812au-dkms deb misc optional arch=all
Checksums-Sha1:
 06375649ccc816d17e493ce7542d831687844d1a 1873112 rtl8812au_5.13.6+git20220728.orig.tar.xz
 3ca36534e29c14f17942e1598a0316fb257c072c 2252 rtl8812au_5.13.6+git20220728-0~peppermint1.debian.tar.xz
Checksums-Sha256:
 f090691711c35e2a5a667f6e940773d79f7bf7c17d0430dee2bafcb2038ca7ae 1873112 rtl8812au_5.13.6+git20220728.orig.tar.xz
 4f06d15a1cceb8bf57a703bedc37369a65d1e2ba1b08d4b16ad6de074f44f78b 2252 rtl8812au_5.13.6+git20220728-0~peppermint1.debian.tar.xz
Files:
 4bd891c1b688a4daa48683c6f741ad84 1873112 rtl8812au_5.13.6+git20220728.orig.tar.xz
 ae9f2110f376abe714b644aa8a028fc7 2252 rtl8812au_5.13.6+git20220728-0~peppermint1.debian.tar.xz
