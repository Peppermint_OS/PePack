Format: 3.0 (quilt)
Source: rtl8814au
Binary: rtl8814au-dkms
Architecture: all
Version: 5.8.5.1-1~peppermint1
Maintainer: SwampRabbit <swamprabbit@mxlinux.org>
Homepage: http://www.realtek.com.tw/
Standards-Version: 3.9.6
Vcs-Browser: https://github.com/coolshou/rtl8814au
Vcs-Git: https://github.com/coolshou/rtl8814au.git
Build-Depends: debhelper (>= 9), dkms, git, sed
Package-List:
 rtl8814au-dkms deb kernel optional arch=all
Checksums-Sha1:
 3a27aee1593bbd3c5c771957dd66639a885a11ac 2430189 rtl8814au_5.8.5.1.orig.tar.gz
 e8c823f370649ca0671425f0133d24e68069b7de 4276 rtl8814au_5.8.5.1-1~peppermint1.debian.tar.xz
Checksums-Sha256:
 fcf603ef90a1de89a59529173fa98d91b11535f4f155b6280daf285d02513dac 2430189 rtl8814au_5.8.5.1.orig.tar.gz
 1dff4b138b8610fcc34230ce27c210c45b00eab146411f56e6551ff5cdae0d84 4276 rtl8814au_5.8.5.1-1~peppermint1.debian.tar.xz
Files:
 be16b85c107603394de869899770b1c7 2430189 rtl8814au_5.8.5.1.orig.tar.gz
 91fa5f81733516f4897fa34efe65f70f 4276 rtl8814au_5.8.5.1-1~peppermint1.debian.tar.xz
Original-Maintainer: jimmy <jimmy@jimmy-alpha>
