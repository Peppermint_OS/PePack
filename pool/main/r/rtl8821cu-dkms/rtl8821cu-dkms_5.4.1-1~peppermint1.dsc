Format: 3.0 (quilt)
Source: rtl8821cu-dkms
Binary: rtl8821cu-dkms, rtl8821cu-src
Architecture: all
Version: 5.4.1-1~peppermint1
Maintainer: SwampRabbit <swamprabbit@mxlinux.org>
Standards-Version: 4.3.0
Vcs-Browser: https://github.com/brektrou/rtl8821CU
Vcs-Git: https://github.com/brektrou/rtl8821CU.git
Build-Depends: debhelper (>= 10), dkms, bc, linux-kbuild-4.19 | linux-kbuild-5.10
Package-List:
 rtl8821cu-dkms deb misc optional arch=all
 rtl8821cu-src deb misc optional arch=all
Checksums-Sha1:
 00c36d62684ef58fdd778db358fdf5d6b8116f20 2244296 rtl8821cu-dkms_5.4.1.orig.tar.xz
 ca93865d48079f50a5d492e0e49130b4e703b5ef 3668 rtl8821cu-dkms_5.4.1-1~peppermint1.debian.tar.xz
Checksums-Sha256:
 a545aa371d4c24ea780deb99844b22a946faf4164958b2a4f5700898313753e7 2244296 rtl8821cu-dkms_5.4.1.orig.tar.xz
 5b4d816ae1fe37cdb2d8a1987a493afbcb9a43f7780d363fab6e5d3d01becb12 3668 rtl8821cu-dkms_5.4.1-1~peppermint1.debian.tar.xz
Files:
 a06b91d81785329444a02acce3e776a3 2244296 rtl8821cu-dkms_5.4.1.orig.tar.xz
 235d9da306de4501cf6cb90335ba674c 3668 rtl8821cu-dkms_5.4.1-1~peppermint1.debian.tar.xz
Original-Maintainer: Marc Leeman <m.leeman@televic.com>
