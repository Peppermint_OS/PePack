Format: 3.0 (quilt)
Source: rtl8821ce
Binary: rtl8821ce-dkms
Architecture: all
Version: 5.5.2.1-7~peppermint1
Maintainer: Steven Pusser <stevep@mxlinux.org>
Homepage: https://launchpad.net/ubuntu/+source/rtl8821ce
Standards-Version: 4.5.0
Vcs-Browser: https://git.launchpad.net/~canonical-hwe-team/ubuntu/+source/rtl8821ce-dkms/+git/rtl8821ce-dkms
Vcs-Git: https://git.launchpad.net/~canonical-hwe-team/ubuntu/+source/rtl8821ce-dkms/+git/rtl8821ce-dkms
Testsuite: autopkgtest
Testsuite-Triggers: gcc, linux-headers, linux-headers-686-pae, linux-headers-amd64, linux-headers-generic
Build-Depends: debhelper-compat (= 12), dh-modaliases, dkms, quilt
Package-List:
 rtl8821ce-dkms deb kernel optional arch=all
Checksums-Sha1:
 701fcbc6d3804be954a00b3cdd838191d6b08b15 3653685 rtl8821ce_5.5.2.1.orig.tar.gz
 f28f866dea7a43b8181859a323aaff6f33172ef6 12784 rtl8821ce_5.5.2.1-7~peppermint1.debian.tar.xz
Checksums-Sha256:
 c619affed9578a602ef0a3e262992ab73c0d04384f6009d27fd00c0909c02c7b 3653685 rtl8821ce_5.5.2.1.orig.tar.gz
 4e50999f2d03e7d627e422a5d10ea29315821c930c537511d5add104d2fb6b10 12784 rtl8821ce_5.5.2.1-7~peppermint1.debian.tar.xz
Files:
 7124e4c1e74029a6dec602744376aa00 3653685 rtl8821ce_5.5.2.1.orig.tar.gz
 ea73abeab65e9bad4a157eeb90d7d8d1 12784 rtl8821ce_5.5.2.1-7~peppermint1.debian.tar.xz
Original-Maintainer: Canonical HWE Team <canonical-hwe@lists.canonical.com>
