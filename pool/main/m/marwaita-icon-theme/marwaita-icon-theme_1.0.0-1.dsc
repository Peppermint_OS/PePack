Format: 3.0 (quilt)
Source: marwaita-icon-theme
Binary: marwaita-icon-theme
Architecture: any
Version: 1.0.0-1
Maintainer: Manuel Rosa <manuelsilvarosa@gmail.com>
Homepage: https://github.com/darkomarko42/Marwaita-Icons
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 marwaita-icon-theme deb misc optional arch=any
Checksums-Sha1:
 41b83a5505c7ce68e6fa85dc2bc3b9b7e814b684 2542216 marwaita-icon-theme_1.0.0.orig.tar.xz
 a6c861586107b850f6ef4b9ec37a836e771f6197 12444 marwaita-icon-theme_1.0.0-1.debian.tar.xz
Checksums-Sha256:
 6368f1866a4312d9267ce472a382ed574d4f4350218b71c7e7f8176f0c884c16 2542216 marwaita-icon-theme_1.0.0.orig.tar.xz
 e6e0a3f0a0312067b7542136b37b56c75a62a5be00e702038cd4388c01149497 12444 marwaita-icon-theme_1.0.0-1.debian.tar.xz
Files:
 a309d6d75213afe653449a8eb516d38b 2542216 marwaita-icon-theme_1.0.0.orig.tar.xz
 ed161c17bc1f603ce8e9d58c16c4b12b 12444 marwaita-icon-theme_1.0.0-1.debian.tar.xz
