Format: 3.0 (quilt)
Source: marwaita-for-xfwm
Binary: marwaita-for-xfwm
Architecture: all
Version: 1.0.2-1-peppermint
Maintainer: Manuel Rosa <manuelrosa@acoroslinux.tk>
Homepage: https://gitlab.com/wednesbunny/marwaita-for-xfwm
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 marwaita-for-xfwm deb misk optional arch=all
Checksums-Sha1:
 4732c00a6c36c791d4ff6f6d8be15cc1107a115e 12044 marwaita-for-xfwm_1.0.2-1.orig.tar.xz
 626d2efdd7fe22e53865b65fc2826b33ce15a0e5 13244 marwaita-for-xfwm_1.0.2-1-peppermint.debian.tar.xz
Checksums-Sha256:
 ce82fcc56b1e7ab08228ccf68f271f46e7dbcb6d8258799dd0d09361ecf37eec 12044 marwaita-for-xfwm_1.0.2-1.orig.tar.xz
 63b58c9f3cec1b030fba96ff08f96f78a00fa17f4ade79fd8d012e1b7b32c9f5 13244 marwaita-for-xfwm_1.0.2-1-peppermint.debian.tar.xz
Files:
 8a18f359ecd08d8c1e05021db2baa5ad 12044 marwaita-for-xfwm_1.0.2-1.orig.tar.xz
 15019ea2b0c8ffffbaa5e8417995ddee 13244 marwaita-for-xfwm_1.0.2-1-peppermint.debian.tar.xz
