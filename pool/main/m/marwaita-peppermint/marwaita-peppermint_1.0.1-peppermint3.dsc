Format: 3.0 (quilt)
Source: marwaita-peppermint
Binary: marwaita-peppermint-gtk-theme
Architecture: all
Version: 1.0.1-peppermint3
Maintainer: Manuel Rosa <manuelsilvarosa@gmail.com>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 marwaita-peppermint-gtk-theme deb misc optional arch=all
Checksums-Sha1:
 578c4f79798d0846280036d8829f57d8739d33e6 56100 marwaita-peppermint_1.0.1.orig.tar.xz
 4ccb25ba24a57f276494b8cb0630f248336ffe60 12568 marwaita-peppermint_1.0.1-peppermint3.debian.tar.xz
Checksums-Sha256:
 7a2fd4bc4e39abbda769fb0ca59779b835c4a7ff9246b9ec184508a5292c1cbc 56100 marwaita-peppermint_1.0.1.orig.tar.xz
 dd490b13a3684525114bb7919f9988ac53142549f88e007591633e575041810c 12568 marwaita-peppermint_1.0.1-peppermint3.debian.tar.xz
Files:
 80b8e88b512c8b4e1f7f8781325ae397 56100 marwaita-peppermint_1.0.1.orig.tar.xz
 900679625fc205b3af916e5fd100418c 12568 marwaita-peppermint_1.0.1-peppermint3.debian.tar.xz
