Format: 3.0 (native)
Source: meta-gnome3
Binary: gnome-core, gnome, gnome-games, gnome-platform-devel, gnome-devel, gnome-api-docs
Architecture: linux-any all
Version: 2:43+1peppermint3
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Jeremy Bicha <jbicha@ubuntu.com>, Laurent Bigonville <bigon@debian.org>
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/gnome-team/meta-gnome3
Vcs-Git: https://salsa.debian.org/gnome-team/meta-gnome3.git
Build-Depends: debhelper-compat (= 13), dh-sequence-gnome
Package-List:
 gnome deb metapackages optional arch=linux-any
 gnome-api-docs deb doc optional arch=all
 gnome-core deb metapackages optional arch=linux-any
 gnome-devel deb devel optional arch=all
 gnome-games deb metapackages optional arch=all
 gnome-platform-devel deb devel optional arch=all
Checksums-Sha1:
 4c6ec67acf799075d55d8ebbed9efba12d61dad9 26800 meta-gnome3_43+1peppermint3.tar.xz
Checksums-Sha256:
 290124e8889ac23efce53714622fe43f65ab9f5792b40a0d7874d13142c14e2d 26800 meta-gnome3_43+1peppermint3.tar.xz
Files:
 fae94e5fee40ee23142cf4241378dbde 26800 meta-gnome3_43+1peppermint3.tar.xz
