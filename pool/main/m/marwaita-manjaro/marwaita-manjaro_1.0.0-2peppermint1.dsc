Format: 3.0 (quilt)
Source: marwaita-manjaro
Binary: marwaita-manjaro
Architecture: all
Version: 1.0.0-2peppermint1
Maintainer: Manuel Rosa <manuelsilvarosa@gmail.com>
Homepage: https://github.com/darkomarko42/Marwaita-manjaro
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 marwaita-manjaro deb misc optional arch=all
Checksums-Sha1:
 4281588831a6cc0791c3d3134c0cefa5684a988b 53400 marwaita-manjaro_1.0.0.orig.tar.xz
 6ceeb450d62be386da83f999f103997f90bd1156 12508 marwaita-manjaro_1.0.0-2peppermint1.debian.tar.xz
Checksums-Sha256:
 99a25939c1c3ceed356468fc11262a2531c37096ab28617c854e2d6760e7d390 53400 marwaita-manjaro_1.0.0.orig.tar.xz
 ce4e84541c8527261b835df894aaf993d84df2f8a8a85ce651f23707a27696f0 12508 marwaita-manjaro_1.0.0-2peppermint1.debian.tar.xz
Files:
 7f874de282df70b35618211a160e7bf2 53400 marwaita-manjaro_1.0.0.orig.tar.xz
 953814df2a808b637302b48856dd0514 12508 marwaita-manjaro_1.0.0-2peppermint1.debian.tar.xz
