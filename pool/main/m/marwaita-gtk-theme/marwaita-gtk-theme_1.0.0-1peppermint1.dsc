Format: 3.0 (quilt)
Source: marwaita-gtk-theme
Binary: marwaita-gtk-theme
Architecture: all
Version: 1.0.0-1peppermint1
Maintainer: Manuel Rosa <manuelsilvarosa@gmail.com>
Standards-Version: 4.5.1
Build-Depends: debhelper-compat (= 13)
Package-List:
 marwaita-gtk-theme deb misc optional arch=all
Checksums-Sha1:
 13399c22b2e313819f5e7fbbff6a4fa17278ff2b 53388 marwaita-gtk-theme_1.0.0.orig.tar.xz
 732d0774a45c25f8df9e73e3cfdebccabcc5c861 12488 marwaita-gtk-theme_1.0.0-1peppermint1.debian.tar.xz
Checksums-Sha256:
 bc4c262e6ed2b29dc6e8e9e73d104c173767f83cd319d22896fbeccaf84023f4 53388 marwaita-gtk-theme_1.0.0.orig.tar.xz
 caf64cf8f98454b668745d555da94d6bfe0ce58f3bdc30b745d9b271254db178 12488 marwaita-gtk-theme_1.0.0-1peppermint1.debian.tar.xz
Files:
 4a801ec3bfcba8b76278981e7f89264c 53388 marwaita-gtk-theme_1.0.0.orig.tar.xz
 573412c05122dc735c1bea4e0abe19d0 12488 marwaita-gtk-theme_1.0.0-1peppermint1.debian.tar.xz
