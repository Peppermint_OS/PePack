Format: 3.0 (quilt)
Source: hplip
Binary: hplip, hpijs-ppds, hplip-data, hplip-doc, hplip-gui, libhpmud-dev, libhpmud0, libsane-hpaio, printer-driver-hpcups, printer-driver-hpijs, printer-driver-postscript-hp
Architecture: any all
Version: 3.21.4+dfsg0-1~peppermint1
Maintainer: Steven Pusser <stevep@mxlinux.org>
Uploaders: Didier Raboud <odyx@debian.org>, Till Kamppeter <till.kamppeter@gmail.com>
Homepage: https://developers.hp.com/hp-linux-imaging-and-printing
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/printing-team/hplip
Vcs-Git: https://salsa.debian.org/printing-team/hplip.git
Testsuite: autopkgtest
Testsuite-Triggers: cups, cups-client
Build-Depends: autoconf, automake, cups, debhelper-compat (= 13), dh-python, fdupes, libavahi-client-dev, libavahi-core-dev, libcups2-dev, libcupsimage2-dev, libdbus-1-dev, libjpeg-dev, libsane-dev, libsnmp-dev, libssl-dev, libtool, libudev-dev [linux-any], libusb-1.0-0-dev [!kfreebsd-any], libusb2-dev [kfreebsd-any], patch, policykit-1, pyppd, pyqt5-dev-tools, python3-dbus, python3-dev, python3-gi, python3-pyqt5
Package-List:
 hpijs-ppds deb utils optional arch=all
 hplip deb utils optional arch=any
 hplip-data deb utils optional arch=all
 hplip-doc deb doc optional arch=all
 hplip-gui deb utils optional arch=all
 libhpmud-dev deb libdevel optional arch=any
 libhpmud0 deb libs optional arch=any
 libsane-hpaio deb libs optional arch=any
 printer-driver-hpcups deb text optional arch=any
 printer-driver-hpijs deb text optional arch=any
 printer-driver-postscript-hp deb utils optional arch=any
Checksums-Sha1:
 90736fc1e1b457a955986c506e251f8e165eb749 9339680 hplip_3.21.4+dfsg0.orig.tar.xz
 d8983daf64c972aa355f5d03caf073483c144a02 138808 hplip_3.21.4+dfsg0-1~peppermint1.debian.tar.xz
Checksums-Sha256:
 3961cc297b835a293cf5d24b58ab07de544503e165d292d324e2121969483c23 9339680 hplip_3.21.4+dfsg0.orig.tar.xz
 42b7ffd368c37f162e742fe7a30f76e2637e71daff2e8969970f3c52088516a5 138808 hplip_3.21.4+dfsg0-1~peppermint1.debian.tar.xz
Files:
 6c2d98c081c2395d5cec44412e74f3ed 9339680 hplip_3.21.4+dfsg0.orig.tar.xz
 3430d43ae94e64ab60795ee1d3f96a8c 138808 hplip_3.21.4+dfsg0-1~peppermint1.debian.tar.xz
Original-Maintainer: Debian Printing Team <debian-printing@lists.debian.org>
