-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (native)
Source: devuan-keyring
Binary: devuan-keyring, devuan-keyring-udeb
Architecture: all
Version: 2022.09.04
Maintainer: Veteran UNIX Admins <vua@devuan.org>
Uploaders: Franco (nextime) Lanza <nextime@devuan.org>, Denis (jaromil) Roio <jaromil@dyne.org>
Homepage: http://www.devuan.org
Standards-Version: 3.9.6
Build-Depends: debhelper (>= 9), gnupg, debian-keyring
Package-List:
 devuan-keyring deb misc important arch=all
 devuan-keyring-udeb udeb debian-installer optional arch=all
Checksums-Sha1:
 a96f53d3bebce52900a11c638ab20eb8b46ea369 53028 devuan-keyring_2022.09.04.tar.xz
Checksums-Sha256:
 9a1be797e65fb57ca18f3aa15f0f90b326f53e0c455f5ca1bad09b8693b50b4a 53028 devuan-keyring_2022.09.04.tar.xz
Files:
 2f49ab755cde525a729fcedb725df8a2 53028 devuan-keyring_2022.09.04.tar.xz

-----BEGIN PGP SIGNATURE-----

iQEzBAEBCgAdFiEEcuPLdzMV36LkZHQ9lFMhJFQZIvsFAmMTZxgACgkQlFMhJFQZ
IvuN+gf+JH4w6nWoB6jWi33W9d77HcmRZjPkACmp8KIECMu2+BG8miWF6YlNaXcF
Y29c4nqlkFNc/5SQfOt+IcLrQhlBi7UQfkSBO2rQFEeouu+8XY+pyF2RGEdejVcG
mHn3TWZIH2SnLl3619NebjxDPZxHwo66vBQf7Ld9fuLzpkX68g+7QD4J2ZvUZuvs
GoBu1PagMV7d4n8jn+c8GytWcinzF5SiU1kBtp5sEXoTTdknO8/8wGR4FXyk3F9J
1aQp875Mmk5tfVYYufafJo9i1cQ6YvFrkgqQp4UMoxokuYapSoSxb7vR6wjKuysF
PM/i9X3hW0eRyY0nnbmA41l5kLKNzw==
=QfjA
-----END PGP SIGNATURE-----
