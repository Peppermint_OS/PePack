Format: 3.0 (native)
Source: debootstrap
Binary: debootstrap, debootstrap-udeb
Architecture: all
Version: 1:1.0.128+peppermint2
Maintainer: Debian Install System Team <debian-boot@lists.debian.org>
Uploaders: Colin Watson <cjwatson@debian.org>, Steve McIntyre <93sam@debian.org>, Hideki Yamane <henrich@debian.org>,
Standards-Version: 4.6.0.1
Vcs-Browser: https://salsa.debian.org/installer-team/debootstrap
Vcs-Git: https://salsa.debian.org/installer-team/debootstrap.git
Testsuite: autopkgtest
Testsuite-Triggers: ca-certificates, libdistro-info-perl, libdpkg-perl, libipc-run-perl, perl, python3-debian, python3-flask, python3-requests, systemd, systemd-container
Build-Depends: debhelper-compat (= 13)
Package-List:
 debootstrap deb admin optional arch=all
 debootstrap-udeb udeb debian-installer optional arch=all
Checksums-Sha1:
 d311392e1ab30697493822a6f40b65d04f85dd15 82500 debootstrap_1.0.128+peppermint2.tar.gz
Checksums-Sha256:
 90428e8cae0c28c399b42ddbd15efd6c41a807b13137995b8d38b299f7562d7e 82500 debootstrap_1.0.128+peppermint2.tar.gz
Files:
 e7d325d86d568de27cdd45c7658dfe9d 82500 debootstrap_1.0.128+peppermint2.tar.gz
