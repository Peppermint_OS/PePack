Format: 3.0 (native)
Source: desktop-base
Binary: desktop-base
Architecture: all
Version: 1:11.0.4~peppermint7
Maintainer: Debian Desktop Team <debian-desktop@lists.debian.org>
Uploaders: Yves-Alexis Perez <corsac@debian.org>, Aurélien COUDERC <coucouf@debian.org>, Jonathan Carter <jcc@debian.org>
Homepage: https://www.debian.org/devel/debian-desktop/
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/debian-desktop-team/desktop-base
Vcs-Git: https://salsa.debian.org/debian-desktop-team/desktop-base.git
Testsuite: autopkgtest
Testsuite-Triggers: libxml2-utils
Build-Depends: debhelper (>= 13), librsvg2-bin, optipng, fonts-quicksand
Package-List:
 desktop-base deb x11 optional arch=all
Checksums-Sha1:
 ac54dd5c49e98f57237db9d30801a0152f0fec73 4342528 desktop-base_11.0.4~peppermint7.tar.xz
Checksums-Sha256:
 34ed34510e2e98dac6f6dfea243d2b42def04b5235ac671c61c37f0ccde82017 4342528 desktop-base_11.0.4~peppermint7.tar.xz
Files:
 24fec1c9987caee8b30669a0bc2075db 4342528 desktop-base_11.0.4~peppermint7.tar.xz
