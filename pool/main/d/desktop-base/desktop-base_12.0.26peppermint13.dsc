Format: 3.0 (native)
Source: desktop-base
Binary: desktop-base
Architecture: all
Version: 1:12.0.26peppermint13
Maintainer: Debian Desktop Team <debian-desktop@lists.debian.org>
Uploaders: Yves-Alexis Perez <corsac@debian.org>, Aurélien COUDERC <coucouf@debian.org>, Jonathan Carter <jcc@debian.org>
Homepage: https://www.debian.org/devel/debian-desktop/
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/debian-desktop-team/desktop-base
Vcs-Git: https://salsa.debian.org/debian-desktop-team/desktop-base.git
Testsuite: autopkgtest
Testsuite-Triggers: libxml2-utils
Build-Depends: debhelper (>= 13), inkscape, librsvg2-bin, optipng, fonts-quicksand
Package-List:
 desktop-base deb x11 optional arch=all
Checksums-Sha1:
 10b18e9764ee54efd087c7e963ed1c4b2ec61b01 9783220 desktop-base_12.0.26peppermint13.tar.xz
Checksums-Sha256:
 f2ae2a18ecdc0a19a7e1b5c90823741537a7121fd40c7f7076d1f5dab766a2f1 9783220 desktop-base_12.0.26peppermint13.tar.xz
Files:
 4d118a5d9edbaf947b64151b074d0a50 9783220 desktop-base_12.0.26peppermint13.tar.xz
