Format: 3.0 (native)
Source: desktop-base
Binary: desktop-base
Architecture: all
Version: 1:12.0.26peppermint14
Maintainer: Debian Desktop Team <debian-desktop@lists.debian.org>
Uploaders: Yves-Alexis Perez <corsac@debian.org>, Aurélien COUDERC <coucouf@debian.org>, Jonathan Carter <jcc@debian.org>
Homepage: https://www.debian.org/devel/debian-desktop/
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/debian-desktop-team/desktop-base
Vcs-Git: https://salsa.debian.org/debian-desktop-team/desktop-base.git
Testsuite: autopkgtest
Testsuite-Triggers: libxml2-utils
Build-Depends: debhelper (>= 13), inkscape, librsvg2-bin, optipng, fonts-quicksand
Package-List:
 desktop-base deb x11 optional arch=all
Checksums-Sha1:
 d9dfcd854d606d8275ec874d9442118762298599 10122640 desktop-base_12.0.26peppermint14.tar.xz
Checksums-Sha256:
 95f18c8d7648e40a11eae8f7f8ee4bac7c9ea79357151886d8e56b2dfaa8148f 10122640 desktop-base_12.0.26peppermint14.tar.xz
Files:
 1afbd9326cf13893c5d6fd2e6cca8338 10122640 desktop-base_12.0.26peppermint14.tar.xz
