Format: 3.0 (quilt)
Source: intel-gpu-tools
Binary: intel-gpu-tools
Architecture: amd64 i386
Version: 1.26-1~peppermint1
Maintainer: SwampRabbit <swamprabbit@mxlinux.org>
Uploaders: Eric Anholt <eric@anholt.net>, Tormod Volden <debian.tormod@gmail.com>, Cyril Brulebois <kibi@debian.org>, Vincent Cheng <vcheng@debian.org>, Jordan Justen <jordan.l.justen@intel.com>
Homepage: https://01.org/linuxgraphics/
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/xorg-team/app/intel-gpu-tools
Vcs-Git: https://salsa.debian.org/xorg-team/app/intel-gpu-tools.git
Build-Depends: bison, debhelper-compat (= 13), flex, gtk-doc-tools, libcairo2-dev, libdrm-dev (>= 2.4.75), libdw-dev, libkmod-dev, libpciaccess-dev (>= 0.10), libprocps-dev, libudev-dev, libunwind-dev, libxrandr-dev, libxv-dev, liboping-dev, meson (>= 0.47), peg, pkg-config, python3-docutils, quilt, x11proto-dev, xutils-dev (>= 1:7.6+6)
Package-List:
 intel-gpu-tools deb x11 optional arch=amd64,i386
Checksums-Sha1:
 40a3f63e6c33d5fc4fc1325c4eaa9c9ea46a54ad 1573468 intel-gpu-tools_1.26.orig.tar.xz
 a195f558410e286313bab3f719cf52e6eb6c35ac 833 intel-gpu-tools_1.26.orig.tar.xz.asc
 a2d12cea370fc9cb0ab1f5c31ea0ec4c384ccc61 12668 intel-gpu-tools_1.26-1~peppermint1.debian.tar.xz
Checksums-Sha256:
 36d4193b9f22fbb4834ec97be3bb6322ec901e20f7be018f0a50d3eb03ec9bb7 1573468 intel-gpu-tools_1.26.orig.tar.xz
 e5b57b67b067ddec8d5f25f1efa742ce78a73af819f19e05f7656863e437ac43 833 intel-gpu-tools_1.26.orig.tar.xz.asc
 42b4a79b42bfd930d02c75b24536416790bfb21c7cef2f06ca098af3ea6dadf3 12668 intel-gpu-tools_1.26-1~peppermint1.debian.tar.xz
Files:
 6781293a2c312bb625aa5cbe5506a9bf 1573468 intel-gpu-tools_1.26.orig.tar.xz
 9a316c4be1c17f6ee2386d64cf602c1e 833 intel-gpu-tools_1.26.orig.tar.xz.asc
 8d454345fe8b4b327462b93533c82071 12668 intel-gpu-tools_1.26-1~peppermint1.debian.tar.xz
Original-Maintainer: Debian X Strike Force <debian-x@lists.debian.org>
