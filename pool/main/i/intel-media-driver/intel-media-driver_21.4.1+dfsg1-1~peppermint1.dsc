Format: 3.0 (quilt)
Source: intel-media-driver
Binary: intel-media-va-driver, libigfxcmrt7, libigfxcmrt-dev
Architecture: amd64 i386 x32
Version: 21.4.1+dfsg1-1~peppermint1
Maintainer: SwampRabbit <swamprabbit@mxlinux.org>
Uploaders:  Sebastian Ramacher <sramacher@debian.org>
Homepage: https://github.com/intel/media-driver
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/multimedia-team/intel-media-driver
Vcs-Git: https://salsa.debian.org/multimedia-team/intel-media-driver.git
Build-Depends: debhelper-compat (= 13), dh-sequence-libva, cmake, libigdgmm-dev (>= 21.1.1), libva-dev (>= 2.12), libx11-dev, pkg-config
Package-List:
 intel-media-va-driver deb video optional arch=amd64,i386,x32
 libigfxcmrt-dev deb libdevel optional arch=amd64,i386,x32
 libigfxcmrt7 deb libs optional arch=amd64,i386,x32
Checksums-Sha1:
 f802af34544514e0de29838fcf24331bad754f55 6526868 intel-media-driver_21.4.1+dfsg1.orig.tar.xz
 e0944b6f6cc2c51a427ce5c5b50949c10154f0ec 6360 intel-media-driver_21.4.1+dfsg1-1~peppermint1.debian.tar.xz
Checksums-Sha256:
 21706f2e727129737d1d9cd93a75b1501a08040a419e8ee029b4d9c706b72e73 6526868 intel-media-driver_21.4.1+dfsg1.orig.tar.xz
 4fdfe7cc6ca150342026882928649556af0b6de1556365e9334521b6227eec52 6360 intel-media-driver_21.4.1+dfsg1-1~peppermint1.debian.tar.xz
Files:
 ded4d78583d741288a2f68b5292987ec 6526868 intel-media-driver_21.4.1+dfsg1.orig.tar.xz
 20ae4cc3d92e2c9b45633cdce958753c 6360 intel-media-driver_21.4.1+dfsg1-1~peppermint1.debian.tar.xz
Original-Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
