Format: 3.0 (quilt)
Source: intel-gmmlib
Binary: libigdgmm11, libigdgmm-dev
Architecture: amd64 i386 x32
Version: 21.3.2+ds1-1~peppermint1
Maintainer: SwampRabbit <swamprabbit@mxlinux.org>
Uploaders:  Sebastian Ramacher <sramacher@debian.org>, Timo Aaltonen <tjaalton@debian.org>
Homepage: https://github.com/intel/gmmlib
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/multimedia-team/intel-gmmlib
Vcs-Git: https://salsa.debian.org/multimedia-team/intel-gmmlib.git
Build-Depends: debhelper-compat (= 13), cmake, libspdlog-dev
Package-List:
 libigdgmm-dev deb libdevel optional arch=amd64,i386,x32
 libigdgmm11 deb libs optional arch=amd64,i386,x32
Checksums-Sha1:
 c6b979601770ac196c57164adc08b661a8d172cd 415528 intel-gmmlib_21.3.2+ds1.orig.tar.xz
 d622366db53c57f63f831b8b77f4e4fe36bc2ce7 4440 intel-gmmlib_21.3.2+ds1-1~peppermint1.debian.tar.xz
Checksums-Sha256:
 3c182a0ed099a9469c82ee056f87ca3fd3d671168a7ddf384cb457ef4357d62a 415528 intel-gmmlib_21.3.2+ds1.orig.tar.xz
 e3ba039d375bb14b07dd568737493a4c48754442a52a83d5e56ff5e2046cae1a 4440 intel-gmmlib_21.3.2+ds1-1~peppermint1.debian.tar.xz
Files:
 5cd63abcde1cbab9ce2f644329194e2b 415528 intel-gmmlib_21.3.2+ds1.orig.tar.xz
 2f997bfcc1a81d0777a69ea18a8d53aa 4440 intel-gmmlib_21.3.2+ds1-1~peppermint1.debian.tar.xz
Original-Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
