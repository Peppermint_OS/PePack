Format: 3.0 (native)
Source: live-build
Binary: live-build
Architecture: all
Version: 1:20230131peppermint1
Maintainer: Debian Live <debian-live@lists.debian.org>
Uploaders: Raphaël Hertzog <raphael@offensive-security.com>, Luca Boccassi <bluca@debian.org>,
Homepage: https://wiki.debian.org/DebianLive
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/live-team/live-build
Vcs-Git: https://salsa.debian.org/live-team/live-build.git
Testsuite: autopkgtest
Testsuite-Triggers: apt-utils, bzip2, ca-certificates, cpio, curl, file, git, wget, xz-utils
Build-Depends: debhelper-compat (= 13), po4a, gettext
Package-List:
 live-build deb misc optional arch=all
Checksums-Sha1:
 a10974116983a264841e066c363496bb6411959f 378408 live-build_20230131peppermint1.tar.xz
Checksums-Sha256:
 6f89906d89b631da128ff5b0a33ceb1612eb1912964bc8c2f74073bfb66be215 378408 live-build_20230131peppermint1.tar.xz
Files:
 4288b246fe57c9d99e3f231bcf29a55a 378408 live-build_20230131peppermint1.tar.xz
