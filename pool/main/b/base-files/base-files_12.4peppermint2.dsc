Format: 3.0 (native)
Source: base-files
Binary: base-files
Architecture: all
Version: 1:12.4peppermint2
Maintainer: Santiago Vila <sanvila@debian.org>
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13), debhelper (>= 13.10~)
Package-List:
 base-files deb admin required arch=all essential=yes
Checksums-Sha1:
 d9aa7a8ef0dc6c5476a40c3701231aeddb1d8d02 66096 base-files_12.4peppermint2.tar.xz
Checksums-Sha256:
 c54726e2836a54fcdc7b3ee0e0e4e6846a8bf67e9aa5c104b4cb6a813a9564e8 66096 base-files_12.4peppermint2.tar.xz
Files:
 afdd32c0ef3936a5aee0c629800419a4 66096 base-files_12.4peppermint2.tar.xz
