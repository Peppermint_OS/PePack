Format: 3.0 (native)
Source: base-files
Binary: base-files
Architecture: all
Version: 1:12.4peppermint3
Maintainer: Santiago Vila <sanvila@debian.org>
Standards-Version: 4.6.2
Build-Depends: debhelper-compat (= 13), debhelper (>= 13.10~)
Package-List:
 base-files deb admin required arch=all essential=yes
Checksums-Sha1:
 16b632b573d1565644e6e9a7c3382798a2f2091f 66124 base-files_12.4peppermint3.tar.xz
Checksums-Sha256:
 b50ce586ac8639e9fb2623ad1bb1d27a9b237b93fda44bc33cfbdf79d6dd8f60 66124 base-files_12.4peppermint3.tar.xz
Files:
 9cc174cebf5c36e36dc338778378120a 66124 base-files_12.4peppermint3.tar.xz
