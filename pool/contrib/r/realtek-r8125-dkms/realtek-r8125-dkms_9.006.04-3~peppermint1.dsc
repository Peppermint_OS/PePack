Format: 3.0 (quilt)
Source: realtek-r8125-dkms
Binary: realtek-r8125-dkms
Architecture: any
Version: 9.006.04-3~peppermint1
Maintainer: SwampRabbit <swamprabbit@mxlinux.org>
Homepage: https://github.com/awesometic/realtek-r8125-dkms
Standards-Version: 4.1.4
Build-Depends: debhelper (>= 11~), dkms
Package-List:
 realtek-r8125-dkms deb contrib/kernel optional arch=any
Checksums-Sha1:
 d8e81a667f063291b11a768a57a934144f4cd47a 126293 realtek-r8125-dkms_9.006.04.orig.tar.gz
 950940485c899f203baaba37d81a1df84b62c665 3272 realtek-r8125-dkms_9.006.04-3~peppermint1.debian.tar.xz
Checksums-Sha256:
 875745687727a47aa5bd2092651faa4e84b401965a65a150d1e4eaf49443bca3 126293 realtek-r8125-dkms_9.006.04.orig.tar.gz
 c2826c2587579f0c5d81a6836909fd4bd270312f543df46168aa425b0e8b7f7f 3272 realtek-r8125-dkms_9.006.04-3~peppermint1.debian.tar.xz
Files:
 adbf016f5f5b8b21b4c7363c0cc45731 126293 realtek-r8125-dkms_9.006.04.orig.tar.gz
 7a3610acef91c6f70797784dcecbfae5 3272 realtek-r8125-dkms_9.006.04-3~peppermint1.debian.tar.xz
Original-Maintainer: Deokgyu Yang <secugyu@gmail.com>
