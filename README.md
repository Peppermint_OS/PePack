Peppermint Linux Repository

This is the official repository for Peppermint Linux, a lightweight and fast Linux distribution based on Debian and Devuan. The repository contains a collection of .deb packages, including those packaged by the Peppermint Linux team, as well as Debian installer images for the AMD64, i386, and ARM64 architectures.
Packages

The repository includes a variety of software packages that are compatible with Peppermint Linux. These packages are provided in .deb format, which is the package format used by Debian-based distributions. You can easily install these packages using the package management tools available in Peppermint Linux, such as apt or dpkg and nala.

Please note that the packages in this repository are released under the GPL3 license. Make sure to review and comply with the terms of the GPL3 license for any software you install from this repository.
Contributing

If you would like to contribute to the Peppermint Linux repository, please follow the guidelines below:

    If you have a package that you would like to submit, please ensure that it is compatible with Peppermint Linux and packaged in .deb format.
    Fork this repository and create a new branch for your package.
    Package your software according to Debian packaging standards, including proper versioning, dependency management, and package metadata.
    Submit a pull request with your package to be reviewed by the Peppermint Linux team.
    Provide proper credits for any third-party software or resources used in your package.

Credits

The Peppermint Linux repository is maintained by the Peppermint Linux team. Credits for specific packages should be attributed to their respective authors and maintainers. Peppermint Linux is a community-driven distribution, and we appreciate the contributions of the open-source community.

For more information about Peppermint Linux, please visit the official website: https://peppermintos.com/